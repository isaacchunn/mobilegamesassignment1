package com.sidm.project1;

public class Node{

    static Node thisNode;
    public boolean walkable;
    public Vector3 worldPosition;
    public int gridX;
    public int gridY;

    public int gCost;
    public int hCost;
    public Node parent;

    public Node(boolean walkable, Vector3 worldPosition, int gridX, int gridY)
    {
        this.walkable = walkable;
        this.worldPosition = worldPosition;
        this.gridX = gridX;
        this.gridY = gridY;

    }
    public void finalize()
    {
        thisNode = this;
    }

    public int fCost()
    {
        return gCost + hCost;
    }

    public int GetGridX()
    {
        return gridX;
    }

    public int GetGridY()
    {
        return gridY;
    }

    public Vector3 GetWorldPosition()
    {
        return worldPosition;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
      } else if(obj == null) {
            return false;
        } else if (obj instanceof  Node) {
            Node tempNode = (Node) obj;
            if (tempNode.GetGridX() == gridX && tempNode.GetGridY() == gridY) {
                return true;
            }
        }
        return false;
    }
}
