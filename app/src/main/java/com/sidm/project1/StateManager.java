package com.sidm.project1;

import android.graphics.Canvas;
import android.view.SurfaceView;

import java.util.HashMap;

public class StateManager {

    //Singleton Instance
    public static final StateManager Instance = new StateManager();

    //Container to store all our states!
    private HashMap<String, StateBase> stateMap = new HashMap<String,StateBase>();

    public StateBase currState = null;
    public StateBase nextState = null;

    private SurfaceView view = null;

    //this is the protected constructor for singleton
    protected StateManager()
    {

    }

    public void Init(SurfaceView _view)
    {
        view = _view;
    }

    void AddState(StateBase _newState)
    {
        //Add the state into the state map
        stateMap.put(_newState.GetName(), _newState);
    }

    void ChangeState(String _nextState)
    {
        //Try to assign the next state
        nextState = stateMap.get(_nextState);

        //If no next state, we assign back to current state
        if(nextState == null)
        {
            nextState = currState;
        }
        //Extra to add if possible : throw some warning if next state function fails
    }

    void Update(float _dt)
    {
        //process all the states changes
        if(currState == null)
        {
            nextState.OnEnter(view);
            currState = nextState;
        }
        else if(nextState != currState )
        {
            //we need to change state
            currState.OnExit();
            nextState.OnEnter(view);
            currState = nextState;
        }

        //Safety catch
        if(currState == null)
        {
            return;
        }
        if(!currState.GetIsPaused())
        {
            currState.Update(_dt); //Update
        }
    }

    void Render(Canvas _canvas)
    {
        currState.Render(_canvas); //Render the state indicated
    }


    String GetCurrentState()
    {
        if(currState == null)
        {
            return "INVALID";
        }

        return currState.GetName();
    }

    void Start(String _newCurrent)
    {
        //Make sure only can call once at the start
        if(currState != null || nextState != null)
        {
            return;
        }

        currState = stateMap.get(_newCurrent);
        if(currState != null)
        {
            currState.OnEnter(view);
            nextState = currState;
        }

    }
}
