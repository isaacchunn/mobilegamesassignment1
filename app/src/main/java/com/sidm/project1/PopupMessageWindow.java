package com.sidm.project1;

import android.app.Activity;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.SearchView;
import android.widget.SeekBar;
import android.widget.Switch;

public class PopupMessageWindow extends Activity implements View.OnClickListener{

    private Button btn_nextScene;
    private Button btn_prevScene;
    private SeekBar volumeSeekbar;
    private Switch soundswitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //to make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // hide title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.popup_message_window);

        //set listener to buttons
        btn_nextScene = (Button)findViewById(R.id.nextScene);
        btn_nextScene.setOnClickListener(this);

        btn_prevScene = (Button)findViewById(R.id.prevScene);
        btn_prevScene.setOnClickListener(this);


        soundswitch = (Switch)findViewById(R.id.soundswitchPopup);
        if(soundswitch.isChecked())
        {
            soundswitch.setChecked(true);
        }
        else
        {
            soundswitch.setChecked(false);
        }
        soundswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    AudioManager.Instance.PlayAudio(R.raw.bensound_littleidea, 0.75f,1);
                    //SoundManager.getInstance().play(getApplicationContext(), R.raw.bensound_littleidea, true);
                }
                else{
                    AudioManager.Instance.StopAudio(R.raw.bensound_littleidea);

                    //SoundManager.getInstance().stop();
                }
            }
        });


        initControls();

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width *0.8) ,(int)(height*0.8));

        //SampleGame.Instance.SetIsPaused(true);
        StateManager.Instance.currState.SetIsPaused(true);
        this.setFinishOnTouchOutside(true);
    }

    @Override
    public void finish() {
        //SampleGame.Instance.SetIsPaused(false);
        StateManager.Instance.currState.SetIsPaused(false);
        super.finish();
    }

    private void initControls () {
        try {
            volumeSeekbar = findViewById(R.id.seekBar);
            volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    // audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,progress,0);
                    //SoundManager.getInstance().player.setVolume(progress*0.01f,progress*0.01f);
                    AudioManager.Instance.SetVolume(R.raw.bensound_littleidea,progress *0.01f);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

        }catch(Exception e)
        {

        }
    }

    @Override
    public void onClick(View v)
    {
        if(v == btn_nextScene)
        {
            StateManager.Instance.ChangeState("Default");
            //StateManager.Instance.currState.SetIsPaused(false);
            finish();
        }
        else if(v == btn_prevScene)
        {
            StateManager.Instance.ChangeState("SampleGame");
            //StateManager.Instance.currState.SetIsPaused(false);
            finish();

        }
    }


}
