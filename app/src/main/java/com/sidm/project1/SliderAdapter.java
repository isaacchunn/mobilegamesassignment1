package com.sidm.project1;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SliderAdapter(Context context)
    {
        this.context = context;
    }

    //Arrays
    public int[] slide_images = {
            R.drawable.a1_start_screen_landscape,
            R.drawable.helptip1,
            R.drawable.helptip,
            R.drawable.fireextinguisher,
            R.drawable.helptip1

    };

    public String[] slide_headings = {
            "Welcome to HZF!",
            "Moving Around",
            "Exclamation Marks",
            "Fire Extinguisher",
            "Have fun HAHA"
    };

    public String[] slide_description = {
            "Your family members are inconsiderate! It is your job to save the earth, by cleaning up after those pesky people.",
            "Tap the screen to move around! You can press the sprint button to move faster as well.",
            "Exclamation marks indicate points of interests. Tend to them quickly by walking to them so you don't lose money.",
            "This is the fire extinguisher. When a fire breaks out, you must use it to tend to fires. However, you can not tend to other hazards while you're holding the extinguisher, so keep that in mind!",
            "Try to survive through the day without losing money! The money at the end will be used as highscore."
    };



    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == (RelativeLayout)o;
    }

    @Override
    public Object instantiateItem( ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slidelayout,container,false);

        ImageView imageView = (ImageView) view.findViewById(R.id.slideImage);
        TextView slideHeading = (TextView)view.findViewById(R.id.slideHeading);
        TextView slideDescription = (TextView)view.findViewById((R.id.slides_description));

        imageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDescription.setText((slide_description[position]));

        container.addView(view);

        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position,Object object) {
        container.removeView((RelativeLayout)object);
    }
}
