package com.sidm.project1;

import android.app.Activity;
import android.content.Entity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceView;

//use collision code for button ecks dee
public class Entity_InteractButton implements EntityBase, Collidable {

    public final static Entity_InteractButton Instance = new Entity_InteractButton(0,0);
    public Vector3 pos;
    boolean isDone = false;
    private boolean isInit = false;
    private Bitmap interactButton = null;
    public boolean active = false;
    GamePage gamePageReference = new GamePage();

    Entity_InteractButton(float xPosition, float yPosition)
    {
        pos = new Vector3(xPosition,yPosition);

    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }
    @Override
    public boolean IsInit() {
        return isInit;
    }
    @Override
    public void Init(SurfaceView _view) {
        interactButton = BitmapFactory.decodeResource(_view.getResources(), R.drawable.interact); //other images
        interactButton = Bitmap.createScaledBitmap(interactButton, GameDataManager.Instance.GetGridSize() * 3, GameDataManager.Instance.GetGridSize() * 3, true);
    }

    static Boolean render = false;
    @Override
    public void Update(float _dt) {
        // Update based on dt
//        render = false;
//        if(CharacterData.Instance.getNearFire())
//        {
//            render = true;
//        }
//
    }

    @Override
    public void Render(Canvas _canvas) {
        if(render) {
            _canvas.drawBitmap(interactButton, pos.x, pos.y, null); //left = x, top = y, counting from top left corner(0,0)
            render = false;
        }
    }

    public static Entity_InteractButton Create(float xPosition, float yPosition)
    {
        //Entity_PauseButton result = new Entity_PauseButton(xPosition,yPosition);
        //EntityManager.Instance.AddEntity(result);
        Entity_InteractButton.Instance.SetPosX(xPosition);
        Entity_InteractButton.Instance.SetPosY(yPosition);
        Entity_InteractButton.Instance.active = true;
        Entity_InteractButton result = Entity_InteractButton.Instance;
        if(EntityManager.Instance.entityList.indexOf(result) == -1 ) //== -1, means not in list
        {
            EntityManager.Instance.AddEntity(result);
        }

        return result;
    }

    @Override
    public String GetType() {
        return "Entity_InteractButton";
    }

    @Override
    public float GetPosX() {
        return pos.x;
    }

    @Override
    public float GetPosY() {
        return pos.y;
    }

    public void SetPosX(float posX){pos.x = posX;}

    public void SetPosY(float posY){pos.y = posY;}

    @Override
    public float GetRadius()
    {
        return interactButton.getHeight()*0.5f;

    }
    @Override
    public void OnHit(Collidable _other)
    {

    }
    @Override
    public void TouchHit()
    {
        if(render) {
            if (CharacterData.Instance.getNearFire()) {
                CharacterData.Instance.holdingFire = !CharacterData.Instance.holdingFire;
                System.out.println(CharacterData.Instance.holdingFire);
            }
        }
    }

    @Override
    public boolean GetAttending()
    {
        return false;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.UI_LAYER;
    }
    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
}
