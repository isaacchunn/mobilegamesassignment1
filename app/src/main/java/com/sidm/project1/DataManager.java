package com.sidm.project1;

import java.util.HashMap;
import java.util.Map;

public class DataManager {

    public final static DataManager Instance = new DataManager();
    private DataManager() {
    }

    private int currentLevel = 0;
    private int numOfMaps = 0;

    //Key == mapNumber;
    //Value is the Data class
    Map<Integer, Data> dataMap = new HashMap<Integer, Data>();

    void Init()
    {
            //Init any data you need here.
    }

    void AddMap(Data data)
    {
        if(dataMap.put(numOfMaps,data) == null) {
            data.mapInstance = numOfMaps;
            System.out.println("Added map/data at position:" + numOfMaps);
            ++numOfMaps;

        }
    }

    Data ReturnData(int mapNumber)
    {
        return dataMap.get(mapNumber);
    }

    int GetCurrentLevel()
    {
        return currentLevel;
    }

    void SetCurrentLevel(int currentLevel)
    {
        this.currentLevel = currentLevel;
    }

    Data ReturnCurrentData()
    {
        return dataMap.get(currentLevel);
    }

}
