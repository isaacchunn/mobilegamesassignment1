package com.sidm.project1;

public class CharacterData {

    public final static CharacterData Instance = new CharacterData();
    private CharacterData() {
    }
     float stamina =0;
     Vector3 position = null;
     float interacting;
     int money = 250;
     boolean holdingFire = false;
     boolean isNearFire = false;
     boolean firingWater = false;
     boolean isOnFire = false;

     String direction = "Right";

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public float getInteracting() {
        return interacting;
    }

    public void setInteracting(float interacting) {
        this.interacting = interacting;
    }

    public float getStamina() {
        return stamina;
    }

    public void setStamina(float stamina) {
        this.stamina = stamina;
    }

    public Vector3 getPosition() {
        return position;
    }

    public void setPosition(Vector3 position) {
        this.position = position;
    }

    public Boolean getNearFire(){
        return this.isNearFire;
    }

    public void setNearFire(Boolean bool)
    {
        this.isNearFire = bool;
    }

    public Boolean getHoldingFire(){
        return this.holdingFire;
    }

    public void setHoldingFire(Boolean bool)
    {
        this.holdingFire = bool;
    }

    public Boolean getFiringWater()
    {
        return this.firingWater;
    }

    public void setFiringWater(Boolean isFire)
    {
        this.firingWater = isFire;
    }

    public String getDirection()
    {
        return this.direction;
    }

    public void setDirection(String direction)
    {
        this.direction = direction;
    }

}