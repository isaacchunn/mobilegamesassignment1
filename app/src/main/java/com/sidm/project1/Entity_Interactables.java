package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

import java.util.Random;

public class Entity_Interactables implements EntityBase, Collidable  {

    //the code become so messy i cant even see thsi anymore - isaac
    //kill me now year 2 sem 2

    public Vector3 pos;
    boolean isDone = false;
    private boolean isInit = false;
    boolean attending = false;
    boolean attended = false;

    private Sprite spriteSheet;
    private Bitmap bitmap;
    private Bitmap bitmap2;

    public  float lightTime = 2;
    public float waterTime = 2;
    public float fireTime = 4;
    public float televisionTime = 2;
    public boolean isTelevisionOn = false;

    //Temporary value for stuff
    public float hp = 100;
    private float maxHP = 100;

    enum TYPE
    {
        WATER,
        LIGHT,
        RUBBISH,
        FIRE,
        TELEVISION,
    }
    TYPE type;

    public Entity_Interactables(float xPosition, float yPosition)
    {
        pos = new Vector3(xPosition,yPosition);
        this.type = TYPE.WATER;
    }
    public Entity_Interactables(float xPosition, float yPosition, TYPE type)
    {
        pos = new Vector3(xPosition,yPosition);
        this.type = type;
    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public boolean IsInit() {
        return isInit;
    }

    @Override
    public void Init(SurfaceView _view) {
        switch(type)
        {
            case LIGHT:
            {
                spriteSheet = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.light),1,4,10);
               break;
            }
            case WATER:
            {
            spriteSheet = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.water), 1,9,14);

                break;
            }
            case RUBBISH:
            {
                Random randome = new Random();
                int result = randome.nextInt(3) + 1;

                switch(result)
                {
                    case 1:
                        bitmap = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rubbish);
                        bitmap = Bitmap.createScaledBitmap(bitmap, GameDataManager.Instance.GetGridSize(), GameDataManager.Instance.GetGridSize(), true);
                        break;
                    case 2:
                        bitmap = BitmapFactory.decodeResource(_view.getResources(), R.drawable.cigarette);
                        bitmap = Bitmap.createScaledBitmap(bitmap, GameDataManager.Instance.GetGridSize(), GameDataManager.Instance.GetGridSize(), true);
                        break;
                    case 3:
                        bitmap = BitmapFactory.decodeResource(_view.getResources(), R.drawable.plasticbag);
                        bitmap = Bitmap.createScaledBitmap(bitmap, GameDataManager.Instance.GetGridSize(), GameDataManager.Instance.GetGridSize(), true);
                        break;
                    default :
                        break;
                }

                break;
            }
            case FIRE:
            {
                spriteSheet = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.fire),10,1,14);
                break;
//                        spriteSheet = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.loading), 4,12,60);
            }
            case TELEVISION:
            {
                bitmap = BitmapFactory.decodeResource(_view.getResources(), R.drawable.televisionon);
                bitmap = Bitmap.createScaledBitmap(bitmap, GameDataManager.Instance.GetGridSize() * 2, GameDataManager.Instance.GetGridSize(), true);

                bitmap2 = BitmapFactory.decodeResource(_view.getResources(), R.drawable.televisionoff);
                bitmap2 = Bitmap.createScaledBitmap(bitmap2, GameDataManager.Instance.GetGridSize() * 2, GameDataManager.Instance.GetGridSize(), true);
            }
        }

    }

    @Override
    public void Update(float _dt) {



        switch(type)
        {
            case WATER:
                spriteSheet.Update(_dt);
                AudioManager.Instance.PlayAudio(R.raw.watersplash,0.5f,1.f);
                if(attending)
                {
                    waterTime -= _dt;
                    CharacterData.Instance.setInteracting(waterTime);
                    if(waterTime <= 0)
                    {
                        attending = false;
                        attended = true;
                        waterTime =0;
                    }
                }
                if(attended)
                {
                    GameDataManager.Instance.RemoveInteractble(this);
                    SetIsDone(true);
                    LevelManager.Instance.booleanMap.put("TAPLEAK", false);
                    attended = false;
                }
                attending = false;
                break;
            case RUBBISH:
                if(attending)
                {
                    Random random = new Random();
                    int rand = random.nextInt(20) + 1;
                    CharacterData.Instance.setMoney(CharacterData.Instance.getMoney() + rand);
                    Texts text = new Texts("+" + rand, 2, new Vector3(pos.x,pos.y + GetRadius()),true);
                    text.setColor(new Color(0,255,0));
                    RenderTextEntity.textList.add(text);
                    GameDataManager.Instance.RemoveInteractble(this);
                    GameDataManager.Instance.SetCurrentRubbish(GameDataManager.Instance.GetCurrentRubbish()-1);
                    attending = false;
                    attended = true;
                }
                if(attended)
                {
                    SetIsDone(true);
                    attended = false;
                }
                break;
            case LIGHT:
                spriteSheet.Update(_dt);
                if(attending)
                {
                    lightTime -= _dt;
                    CharacterData.Instance.setInteracting(lightTime);
                    if(lightTime <= 0)
                    {
                        attending = false;
                        attended = true;
                        lightTime =0;
                    }
                }
                if(attended)
                {
                    GameDataManager.Instance.RemoveInteractble(this);
                    SetIsDone(true);
                    attended = false;
                }
                break;
            case FIRE:
                AudioManager.Instance.PlayAudio(R.raw.firesound,0.5f,1.f);
                CharacterData.Instance.isOnFire = true;
                spriteSheet.Update(_dt);
                if(attending)
                {
                    if(CharacterData.Instance.firingWater)
                    {
                        hp -=15;
                        spriteSheet.SetAlpha((hp/maxHP) * 255);
                        CharacterData.Instance.firingWater = false;
                    }
                }

                if(hp <= 0)
                {
                    GameDataManager.Instance.RemoveInteractble(this);
                    SetIsDone(true);
                    attended = false;
                    LevelManager.Instance.booleanMap.put("FIRE", false);
                    CharacterData.Instance.isOnFire = false;
                }
                break;
            case TELEVISION:
                if(attending)
                {
                    if(isTelevisionOn) {
                        televisionTime -= _dt;
                        CharacterData.Instance.setInteracting(televisionTime);
                        if (televisionTime <= 0) {
                            attending = false;
                            attended = true;
                            televisionTime = 0;
                        }
                    }
                }
                if(attended)
                {
                    GameDataManager.Instance.RemoveInteractble(this);
                    SetIsDone(true);
                    isTelevisionOn = false;
                    attended = false;
                    CreateTelevisionIntoList(pos.x,pos.y,false);
                    LevelManager.Instance.booleanMap.put("TELEVISION", false);
                }
                break;

        }

    }

    @Override
    public void Render(Canvas _canvas) {
        switch(type)
        {
            case WATER:
                spriteSheet.Render(_canvas,GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                break;
            case RUBBISH:
                _canvas.drawBitmap(bitmap,  (int) pos.x,GameDataManager.Instance.GetGridOffset() + (int) pos.y, null); //left = x, top = y, counting from top left corner(0,0)
                break;
            case LIGHT:
                spriteSheet.Render(_canvas,GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                break;
            case FIRE:
                spriteSheet.Render(_canvas,GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                break;
            case TELEVISION:

                if(isTelevisionOn)
                {
                    _canvas.drawBitmap(bitmap,  (int) pos.x,GameDataManager.Instance.GetGridOffset() + (int) pos.y, null); //left = x, top = y, counting from top left corner(0,0)
                }
                else if (!isTelevisionOn)
                {
                    _canvas.drawBitmap(bitmap2,  (int) pos.x,GameDataManager.Instance.GetGridOffset() + (int) pos.y, null); //left = x, top = y, counting from top left corner(0,0)
                }
                break;
        }
        }

    @Override
    public String GetType() {
        switch(type)
        {
            case TELEVISION:
                return "Entity_Television";
            case RUBBISH:
                return "Entity_Rubbish";
            case FIRE:
                return "Entity_Fire";
            case LIGHT:
                return "Entity_Light";
            case WATER:
                return "Entity_Water";
        }


        return "Entity_Interactable";
    }

    @Override
    public float GetPosX() {
        return pos.x;
    }

    @Override
    public float GetPosY() {
        return pos.y;
    }

    @Override
    public float GetRadius() {
        switch(type)
        {
            case WATER:
                return spriteSheet.GetHeight() * 0.5f;
            case RUBBISH:
                return bitmap.getHeight() * 0.5f;
            case LIGHT:
                return spriteSheet.GetHeight() * 0.5f;
            case FIRE:
                return spriteSheet.GetHeight() * 0.5f;
            case TELEVISION: {
                if (isTelevisionOn) {
                    return bitmap.getHeight() * 0.5f;
                }
                return 0;
            }
        }
        return 0;
    }

    @Override
    public void OnHit(Collidable _other) {
        if(_other.GetType() == "Entity_Character")
        {
            if(type == TYPE.TELEVISION)
                {
                    if(CharacterData.Instance.holdingFire == false) {
                        if (isTelevisionOn) {
                            attending = true;
                        } else {
                            attending = false;
                        }
                    }
            }
            else if (type == TYPE.FIRE)
            {
                attending = true;
            }
            else if (type == TYPE.RUBBISH)
            {
                attending = true;
            }
            else
            {
                if(CharacterData.Instance.holdingFire == false) {
                    attending = true;
                }
            }

        }
        else if (_other.GetType() == "Entity_NPC")
        {
                if (type == TYPE.TELEVISION) {
                    if (isTelevisionOn == false) {
                        isTelevisionOn = true;
                        GameDataManager.Instance.AddInteractable((this));
                    }
                }
        }
    }

    @Override
    public void TouchHit() {

    }
    @Override
    public boolean GetAttending()
    {
        return attended;
    }

    public static Entity_Interactables Create(float xPosition, float yPosition) {
        Entity_Interactables result = new Entity_Interactables(xPosition, yPosition);
        EntityManager.Instance.AddEntity(result);
        GameDataManager.Instance.AddInteractable((result));
        return result;
    }

    public static Entity_Interactables Create(float xPosition, float yPosition, TYPE type) {
        Entity_Interactables result = new Entity_Interactables(xPosition, yPosition, type);
        EntityManager.Instance.AddEntity(result);
        GameDataManager.Instance.AddInteractable((result));
        return result;
    }

    public static Entity_Interactables CreateIntoList(float xPosition, float yPosition, TYPE type) {
        Entity_Interactables result = new Entity_Interactables(xPosition, yPosition, type);
        EntityManager.Instance.AddEntityIntoList(result);
        if(type != TYPE.RUBBISH) {
            GameDataManager.Instance.AddInteractable((result));
        }
        return result;
    }

    public static Entity_Interactables CreateTelevisionIntoList(float xPosition, float yPosition, boolean isOn) {
        Entity_Interactables result = new Entity_Interactables(xPosition, yPosition);
        result.type = TYPE.TELEVISION;
        result.isTelevisionOn = isOn;
        EntityManager.Instance.AddEntityIntoList(result);
        if(result.isTelevisionOn) {
            GameDataManager.Instance.AddInteractable((result));
        }

        return result;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.GAMEOBJECTS_LAYER;
    }
    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
}

