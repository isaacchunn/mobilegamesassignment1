package com.sidm.project1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceView;

import org.w3c.dom.Entity;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;

public class MainGameState implements StateBase {


    //Write a singleton
    public final static MainGameState Instance = new MainGameState();

    //Define object name
    public Bitmap backgroundbmp; //pictures like png files are known as bit map
    //Scaled - scaled version based on Screenwidth & height for the image bmp

    //All bitmaps
    public Bitmap emptyBmp; //lenny face icon
    public Bitmap wallBmp; //just black
    public Bitmap exmark; //exclamation mark
    public Bitmap blackQuad; //black quad / just black
    public Bitmap scaledbackgroundbmp; //background bmp
    public Bitmap SemptyBmp;
    public Bitmap SwallBmp;
    public Bitmap scaledExmark;

    private Sprite spriteSheet; //smoke animation
    private Bitmap smokeBmp; //

    private boolean isFire  = false;

    public float elapsedTime = 0;

    int ScreenWidth, ScreenHeight; //screenwidth and height
    float xPos = 0.f, yPos = 0.f; //variable used for positioning of images

    //Containers.
    Data.TILES[] data; //The enum et each grid
    Map<Integer, Boolean> dataMap; // is the place walkable?
    ArrayList<Node> path; // the path the entity is supposed to take.

    private int gridSize, noOfGrid, gridOffset;
    Random seed = new Random();

    public boolean IsPaused = false;
    //DEBUG
    public boolean Debug = false;
public SurfaceView view  = null;
    /*
        public Bitmap button_popup;
    */
    public float timer;
    static float moneyMinusTimer = 1;
    private boolean initialized = false;
    @Override
    public String GetName()
    {
                return "Default";
    }

    @Override
    public void OnEnter(SurfaceView _view)
    {
        EntityManager.Instance.entityList.clear();
        LevelManager.Instance.ClearEntireMap();
        GameDataManager.Instance.interactableList.clear();

        if(MainGameState.Instance.initialized == false)
        {
           //MainGameState.Instance.Init(_view);
            AudioManager.Instance.Init(_view);
        }
       // CharacterData.Instance.money = 200;
        DataManager.Instance.SetCurrentLevel(1);
        Grid.Instance.CreateGrid(1);

        //empty the list first
        //Init the variables
        gridSize = GameDataManager.Instance.GetGridSize();
        noOfGrid = GameDataManager.Instance.GetNoOfGrid();
        gridOffset = GameDataManager.Instance.GetGridOffset();
        ScreenWidth = GameDataManager.Instance.GetScreenWidth();
        ScreenHeight = GameDataManager.Instance.GetScreenHeight();
        //Load Resources for e.g. Images

        backgroundbmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.tilemaplevel2);
        emptyBmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.icon);
        wallBmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.justblack);
        exmark= BitmapFactory.decodeResource(_view.getResources(), R.drawable.exmark);
/*
        button_popup = BitmapFactory.decodeResource(_View.getResources(), R.drawable.buttonxml);
*/
        //Scaled version of bmp which is background of game scene
        backgroundbmp = Bitmap.createScaledBitmap(backgroundbmp, ScreenWidth, ScreenHeight, true);
        exmark = Bitmap.createScaledBitmap(exmark, gridSize * 2, gridSize * 2, true);
        wallBmp = Bitmap.createScaledBitmap(wallBmp, gridSize, gridSize, true);
        emptyBmp = Bitmap.createScaledBitmap(emptyBmp, gridSize, gridSize, true);
        //blackQuad = Bitmap.createScaledBitmap(blackQuad, ScreenWidth, ScreenHeight, true);

        smokeBmp = BitmapFactory.decodeResource(_view.getResources(),R.drawable.smoke);
        smokeBmp = Bitmap.createScaledBitmap(smokeBmp, ScreenWidth * 6 ,ScreenHeight,true);

        spriteSheet = new Sprite(smokeBmp,1,2,3);
        spriteSheet.SetAlpha(150);

        EntityManager.Instance.Init(_view);
        //System.out.println("Grid size: " + GameDataManager.Instance.GetGridSize());

        data = DataManager.Instance.dataMap.get(DataManager.Instance.GetCurrentLevel()).tilesArray;
//        for(int i = 0;i < data.length; ++i)
//        {
//            if(data[i].equals(Data.TILES.SPRITE)) {
//                //System.out.println("Created sample entity. X:" +(i % noOfGrid) * gridSize + "Y: " + (i / noOfGrid) * gridSize);
//                Entity_Character.SampleEntityCreate(gridOffset + (float)(i % noOfGrid) * gridSize, gridOffset +(float)(i / noOfGrid) * gridSize);
//            }
//        }
        dataMap = DataManager.Instance.dataMap.get(DataManager.Instance.GetCurrentLevel()).walkableMap;

        RenderTextEntity.Create();

/*
        //Hardcode first
        Vector3 water = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.84f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.74f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("TAPLEAK",  water);
*/

//        Entity_Interactables.Create(ScreenWidth * 0.15f, ScreenHeight * 0.15f,Entity_Interactables.TYPE.FIRE);
        //Entity_Obstacle.Create(1500, 700);

        //Commented out part begins****************


        Entity_NPC.Create(ScreenWidth*0.9f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.8f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.7f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.6f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.5f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_Button.Create(ScreenWidth*0.9f, ScreenHeight *0.825f);

        //Entity_Obstacle.Create(1500, 700);

        Entity_PauseButton.Create(ScreenWidth*0.9f, ScreenHeight *0.01f);

        RenderBackground.Create();

        Entity_InteractButton.Create(ScreenWidth*0.9f, ScreenHeight *0.60f);;

        Entity_SprayButton.Create(ScreenWidth*0.9f, ScreenHeight *0.825f);;

        Entity_FireExtinguisher.Create(ScreenWidth * 0.2f, ScreenHeight * 0.5f);

//        Entity_Interactables.Create(ScreenWidth * 0.80f, ScreenHeight * 0.45f,Entity_Interactables.TYPE.TELEVISION);


        Entity_Interactables.CreateTelevisionIntoList(ScreenWidth * 0.575f, ScreenHeight * 0.77f,false);
        //Entity_Interactables.CreateTelevisionIntoList(ScreenWidth * 0.41f, ScreenHeight * 0.51f,false);

        //Hardcode first
        Vector3 tele = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.575f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.77f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("TELEVISION",  tele);

/*
        tele = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.41f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.51f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("TELEVISION",  tele);
*/

/*
        Vector3 fire = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.54f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.21f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("FIRE",  fire);
*/

        //Main character
        Entity_Character.Create(ScreenWidth*0.1f, ScreenHeight* 0.47f);
        initialized = true;

        AudioManager.Instance.PlayAudio(R.raw.bensound_littleidea, 0.75f,1);
         view = _view;
    }

    @Override
    public void OnExit()
    {

    }

    public void Init(SurfaceView _View) {

        Grid.Instance.CreateGrid(1);

        AudioManager.Instance.Init(_View);
        CharacterData.Instance.money = 200;

        //Init the variables
        gridSize = GameDataManager.Instance.GetGridSize();
        noOfGrid = GameDataManager.Instance.GetNoOfGrid();
        gridOffset = GameDataManager.Instance.GetGridOffset();
        ScreenWidth = GameDataManager.Instance.GetScreenWidth();
        ScreenHeight = GameDataManager.Instance.GetScreenHeight();
        //Load Resources for e.g. Images

        backgroundbmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.tilemaplevel2);
        emptyBmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.icon);
        wallBmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.justblack);
        exmark= BitmapFactory.decodeResource(_View.getResources(), R.drawable.exmark);
        blackQuad = BitmapFactory.decodeResource(_View.getResources(),R.drawable.black);
        //button_popup = BitmapFactory.decodeResource(_View.getResources(), R.drawable.buttonxml);
//*/
        //Scaled version of bmp which is background of game scene
        backgroundbmp = Bitmap.createScaledBitmap(backgroundbmp, ScreenWidth, ScreenHeight, true);
        exmark = Bitmap.createScaledBitmap(exmark, gridSize * 2, gridSize * 2, true);
        //wallBmp = Bitmap.createScaledBitmap(wallBmp, gridSize, gridSize, true);
        emptyBmp = Bitmap.createScaledBitmap(emptyBmp, gridSize, gridSize, true);
        //blackQuad = Bitmap.createScaledBitmap(blackQuad, ScreenWidth, ScreenHeight, true);

        EntityManager.Instance.Init(_View);
        //System.out.println("Grid size: " + GameDataManager.Instance.GetGridSize());

        data = DataManager.Instance.dataMap.get(DataManager.Instance.GetCurrentLevel()).tilesArray;
//        for(int i = 0;i < data.length; ++i)
//        {
//            if(data[i].equals(Data.TILES.SPRITE)) {
//                //System.out.println("Created sample entity. X:" +(i % noOfGrid) * gridSize + "Y: " + (i / noOfGrid) * gridSize);
//                Entity_Character.SampleEntityCreate(gridOffset + (float)(i % noOfGrid) * gridSize, gridOffset +(float)(i / noOfGrid) * gridSize);
//            }
//        }
        dataMap = DataManager.Instance.dataMap.get(DataManager.Instance.GetCurrentLevel()).walkableMap;

        RenderTextEntity.Create();

/*
        //Hardcode first
        Vector3 water = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.84f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.74f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("TAPLEAK",  water);
*/

//        Entity_Interactables.Create(ScreenWidth * 0.15f, ScreenHeight * 0.15f,Entity_Interactables.TYPE.FIRE);
        //Entity_Obstacle.Create(1500, 700);

        //Commented out part begins****************


        Entity_NPC.Create(ScreenWidth*0.9f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.8f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.7f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.6f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.5f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_Button.Create(ScreenWidth*0.9f, ScreenHeight *0.825f);

        //Entity_Obstacle.Create(1500, 700);

        Entity_PauseButton.Create(ScreenWidth*0.9f, ScreenHeight *0.01f);

        RenderBackground.Create();

        Entity_InteractButton.Create(ScreenWidth*0.9f, ScreenHeight *0.60f);;

        Entity_SprayButton.Create(ScreenWidth*0.9f, ScreenHeight *0.825f);;

        Entity_FireExtinguisher.Create(ScreenWidth * 0.2f, ScreenHeight * 0.5f);

//        Entity_Interactables.Create(ScreenWidth * 0.80f, ScreenHeight * 0.45f,Entity_Interactables.TYPE.TELEVISION);


        Entity_Interactables.CreateTelevisionIntoList(ScreenWidth * 0.575f, ScreenHeight * 0.77f,false);
        //Entity_Interactables.CreateTelevisionIntoList(ScreenWidth * 0.41f, ScreenHeight * 0.51f,false);

        //Hardcode first
        Vector3 tele = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.575f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.77f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("TELEVISION",  tele);

/*
        tele = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.41f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.51f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("TELEVISION2",  tele);
*/

/*
        Vector3 fire = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.54f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.21f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("FIRE",  fire);
*/

        //Main character
        Entity_Character.Create(ScreenWidth*0.1f, ScreenHeight* 0.47f);
        initialized = true;

        AudioManager.Instance.PlayAudio(R.raw.bensound_littleidea, 0.75f,1);
         view = _View;
    }

    @Override
    public void Render(Canvas _canvas)
    {
        /*EntityManager.Instance.Render(_canvas);

        //String scoreText = String.format("SCORE : %d", GameSystem.Instance.GetIntFromSave("Score"));

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(64);

        _canvas.drawText("Supposed to be score but ya", 10,220, paint);*/


        //draw something
        _canvas.drawBitmap(backgroundbmp, xPos, yPos, null); //left = x, top = y, counting from top left corner(0,0)
//
//        for (int i = 0; i < data.length; ++i) {
//            if (data[i] == null) {
//                data[i] = Data.TILES.EMPTY;
//            }
//            switch (data[i]) {
//                case WALLS: {
//                    //  _canvas.drawBitmap(SwallBmp, (i % noOfGrid)* gridSize, (i / noOfGrid) * gridSize, null); //left = x, top = y, counting from top left corner(0,0)
//                    break;
//                }
//                case EMPTY: {
//                    //_canvas.drawBitmap(SemptyBmp,  (i % noOfGrid)* gridSize, (i / noOfGrid) * gridSize, null); //left = x, top = y, counting from top left corner(0,0)
//                    break;
//                }
//                case DEFAULT: {
//                    break;
//                }
//            }
//        }


        if(GameDataManager.Instance.interactableList != null) {
            //Want to render the exclamation above this
            for (Entity_Interactables entity : GameDataManager.Instance.interactableList) {
                if(entity.type != Entity_Interactables.TYPE.RUBBISH) {
                    _canvas.drawBitmap(exmark, entity.pos.x, entity.pos.y - entity.GetRadius(), null); //left = x, top = y, counting from top left corner(0,0)
                }
            }
        }

        //Render entity
        EntityManager.Instance.Render(_canvas);

        //spriteSheet.Render(_canvas,ScreenWidth / 2, ScreenHeight /2);
/*
        Paint paint = new Paint();
        paint.setARGB(155,0,0,0);
        _canvas.drawBitmap(blackQuad, xPos, yPos, paint );
*/

    }

    @Override
    public void Update(float _dt)
    {
     /*   timer -= _dt;
        if(timer <= 0.0f)
        {
            // Entity_NPC.Create(0,0);
            timer = 0.0f;
        }
        EntityManager.Instance.Update(_dt);
*/
        spriteSheet.Update(_dt);

        {
            if (xPos < -ScreenWidth) {
                xPos = 0;
            }

            float hour = GameDataManager.Instance.getHourOfTheDay();
            hour += 1.0f * _dt;
            if (hour >= 24.f) {
                if(DataSaver.Instance.GetIntFromSave("highscorelevel2") < CharacterData.Instance.money) {
                    DataSaver.Instance.SaveEditBegin();
                    DataSaver.Instance.SetIntInSave("highscorelevel2", CharacterData.Instance.money);
                    DataSaver.Instance.SaveEditEnd();
                }
                view.getContext().startActivity(new Intent(view.getContext(), ScreenWin.class));
                hour = 0;
            }
            GameDataManager.Instance.setHourOfTheDay(hour);

            if(CharacterData.Instance.money <=0)
            {
                view.getContext().startActivity(new Intent(view.getContext(), ScreenLose.class));
            }

            elapsedTime += _dt;
            if (elapsedTime > 3.0f) {
                int randPosX = seed.nextInt(ScreenWidth / gridSize);
                int randPosY = seed.nextInt(ScreenHeight / gridSize);

                //Entity_Obstacle.Create(randPosX * gridSize + gridOffset, randPosY * gridSize + gridOffset);
/*
            DataManager.Instance.dataMap.get(0).intArray[(randPosY * noOfGrid + randPosX)] = 2;
            DataManager.Instance.dataMap.get(0).UpdateTileArray();
            DataManager.Instance.dataMap.get(0).UpdateWalkableAreas();
*/
                elapsedTime = 0;
            }
            if (GameDataManager.Instance.interactableList != null) {
                moneyMinusTimer -= _dt;
                if (moneyMinusTimer <= 0) {
                    int money = CharacterData.Instance.getMoney();
                    money -= (GameDataManager.Instance.interactableList.size()) * 3;
                    CharacterData.Instance.setMoney(money);
                    moneyMinusTimer = 1;
                }

            }

            moneyMinusTimer -= _dt;
            if (moneyMinusTimer <= 0) {
                int money = CharacterData.Instance.getMoney();
                money -= 5;
                CharacterData.Instance.setMoney(money);
                moneyMinusTimer = 1;
            }
            EntityManager.Instance.Update(_dt);

            if(isFire)
            {
                float minAlpha = 70.f;
                if(spriteSheet.GetAlpha() > minAlpha)
                {
                    spriteSheet.SetAlpha(spriteSheet.GetAlpha() - _dt);
                }
            }
            else
            {
                float maxAlpha = 120.f;
                if(spriteSheet.GetAlpha() < maxAlpha)
                {
                    spriteSheet.SetAlpha(spriteSheet.GetAlpha() + _dt);
                }
            }

        }
    }

    @Override
    public void SetIsPaused(boolean _pauseStatus)
    {
        IsPaused = _pauseStatus;
        return;
    }

    @Override
    public boolean GetIsPaused()
    {
        return IsPaused;
    }

}
