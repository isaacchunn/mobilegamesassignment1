package com.sidm.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class ScreenLose extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //to make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // hide title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.screenlose);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width *0.8) ,(int)(height*0.8));

        StateManager.Instance.currState.SetIsPaused(true);
        this.setFinishOnTouchOutside(true);

    }
    @Override
    public void finish() {

        //SampleGame.Instance.SetIsPaused(false);
        StateManager.Instance.currState.SetIsPaused(false);
        super.finish();
        startActivity(new Intent(this, Mainmenu.class));

    }

    @Override
    public void onClick(View v)
    {
        finish();

    }
}
