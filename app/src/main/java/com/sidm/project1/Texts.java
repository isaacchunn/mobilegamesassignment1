package com.sidm.project1;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Texts {

    public Vector3 pos;
    public String text;
    public  float  lifeTime;

    public Color color;
    boolean isAlive = false;
    boolean riseEffect = false;
    static final float offsetTime = 150f;
    static final float alphaDecrease = 30f;

    Texts(String text, float lifeTime, Vector3 position) // base constructor
    {
        this.text = text;
        this.lifeTime =lifeTime;
        this.pos = position;
        this.riseEffect = false;
        isAlive = true;
        pos = new Vector3(position.x,position.y,position.z);
        color = new Color();

    }
    Texts(String text, float lifeTime, Vector3 position, boolean riseEffect) //for rise effect
    {
        this.text = text;
        this.lifeTime =lifeTime;
        this.pos = position;
        this.riseEffect = riseEffect;
        isAlive = true;
        pos = new Vector3(position.x,position.y,position.z);
        color = new Color();
    }

    void Update(float deltaTime)
    {
        //Only update if its alive.
        if(isAlive) {
            lifeTime -=   deltaTime;
//            if (lifeTime <= 0) {
//                color.alpha =0;
//                isAlive = false;
//            }
            //Fade out effect
            if(lifeTime <= 1)
            {
                if(color.alpha > 0) {
                    color.alpha -= alphaDecrease;
                    if(color.alpha <= 10) // life hacks
                    {
                        isAlive = false;
                        color.alpha = 0;
                        lifeTime = 0;
                    }
                }
            }
            if (riseEffect) {
                pos.y -= offsetTime * deltaTime; // - to go upwards
            }

        }
    }

    void Render(Canvas _canvas)
    {

    }

    public Vector3 getPos() {
        return pos;
    }

    public void setPos(Vector3 pos) {
        this.pos = pos;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getLifeTime() {
        return lifeTime;
    }

    public void setLifeTime(float lifeTime) {
        this.lifeTime = lifeTime;
    }

    public int getAlpha() {
        return color.alpha;
    }

    public void setAlpha(int alpha) {
        this.color.alpha = alpha;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public boolean isRiseEffect() {
        return riseEffect;
    }

    public void setRiseEffect(boolean riseEffect) {
        this.riseEffect = riseEffect;
    }





}
