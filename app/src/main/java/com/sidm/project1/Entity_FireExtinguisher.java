package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.SurfaceView;

//use collision code for button ecks dee
public class Entity_FireExtinguisher implements EntityBase, Collidable {

    public Vector3 pos;
    boolean isDone = false;
    private boolean isInit = false;
    private Bitmap fireextinguisher = null;

    Entity_FireExtinguisher(float xPosition, float yPosition)
    {
        pos = new Vector3(xPosition,yPosition);
    }

    @Override
    public boolean GetAttending()
    {
        return false;
    }
    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }
    @Override
    public boolean IsInit() {
        return isInit;
    }
    @Override
    public void Init(SurfaceView _view) {
        fireextinguisher = BitmapFactory.decodeResource(_view.getResources(), R.drawable.fireextinguisher); //other images
        fireextinguisher = Bitmap.createScaledBitmap(fireextinguisher, GameDataManager.Instance.GetGridSize() * 2 , GameDataManager.Instance.GetGridSize() * 2 , true);
    }

    @Override
    public void Update(float _dt) {
        // Update based on dt
        if(CharacterData.Instance.holdingFire)
        {
            pos = CharacterData.Instance.position;
        }
    }
    static Entity_Character.MOVEMENT movement;
    @Override
    public void Render(Canvas _canvas) {

        if(CharacterData.Instance.holdingFire) {
            Matrix transform = new Matrix();
            transform.postTranslate(-fireextinguisher.getWidth() * 0.5f, -fireextinguisher.getHeight() * 0.5f);

            switch(CharacterData.Instance.getDirection())
            {
                case "Up":
                    transform.postRotate(90);
                    break;
                case "Down":
                    transform.postRotate(-90);
                    break;
                case "Right":
                    transform.postScale(-1, 1, fireextinguisher.getWidth() * 0.25f, 0);
                    break;
                case "Left":
                    break;
            }
            transform.postTranslate(pos.x, pos.y);

            _canvas.drawBitmap(fireextinguisher, transform, null); //left = x, top = y, counting from  // top left corner(0,0)
        }
        else
        {
            _canvas.drawBitmap(fireextinguisher, pos.x,pos.y, null);
        }
    }

    public static Entity_FireExtinguisher Create(float xPosition, float yPosition)
    {
        Entity_FireExtinguisher result = new Entity_FireExtinguisher(xPosition,yPosition);
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public String GetType() {
        return "Entity_FireExtinguisher";
    }

    @Override
    public float GetPosX() {
        return pos.x;
    }

    @Override
    public float GetPosY() {
        return pos.y;
    }

    @Override
    public float GetRadius()
    {
        return fireextinguisher.getHeight()*0.5f;
    }
    @Override
    public void OnHit(Collidable _other)
    {
        if(_other.GetType() == "Entity_Character")
        {
            CharacterData.Instance.isNearFire = true;
        }
    }
    @Override
    public void TouchHit()
    {
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.GAMEOBJECTS_LAYER;
    }
    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
}
