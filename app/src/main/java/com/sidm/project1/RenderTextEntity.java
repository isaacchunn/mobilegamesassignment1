package com.sidm.project1;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.SurfaceView;

import java.util.ArrayList;


public class RenderTextEntity implements EntityBase {

    // Paint object
    Paint paint = new Paint();
    private int red = 0, green = 0, blue = 0;

    private boolean isDone = false;
    private boolean isInit = false;

    int frameCount;
    long lastTime = 0;
    long lastFPSTime = 0;
    float fps;

    Typeface myfont;

    public static ArrayList<Texts> textList = new ArrayList<>(); // static so i can add at anytime.
    private ArrayList<Texts>removalList = new ArrayList<>();

    @Override
    public boolean IsInit() {
        return isInit;
    }


    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public void Init(SurfaceView _view) {

        // Week 8 Use my own fonts
        myfont = Typeface.createFromAsset(_view.getContext().getAssets(), "fonts/Gemcut.otf");
        isInit = true;
    }

    @Override
    public void Update(float _dt) {

        // get actual fps

        frameCount++;

        long currentTime = System.currentTimeMillis();

        lastTime = currentTime;

        if(currentTime - lastFPSTime > 1000)
        {
            fps = (frameCount * 1000.f) / (currentTime - lastFPSTime);
            lastFPSTime = currentTime;
            frameCount = 0;
        }

        for(Texts text : textList)
        {
            text.Update(_dt);
            if(!text.isAlive)
            {
                removalList.add(text);
            }
        }
        //Remove Texts
        for(Texts text : removalList)
        {
            textList.remove(text);
        }
        removalList.clear();


    }

    @Override
    public void Render(Canvas _canvas)
    {
        Paint paint = new Paint();
        paint.setARGB(255, 0,255,0);
        paint.setStrokeWidth(200);
        paint.setTextSize(70);
        paint.setTypeface(myfont); // changing font
        _canvas.drawText("FPS: " + (int)fps, GameDataManager.Instance.GetScreenWidth() *0.7f, 80, paint);
        _canvas.drawText("Stamina: " + (int)CharacterData.Instance.stamina, 30, GameDataManager.Instance.GetScreenHeight() *0.95f, paint);
        _canvas.drawText("Pos: " + CharacterData.Instance.position.toString(), GameDataManager.Instance.GetScreenWidth()* 0.33f, GameDataManager.Instance.GetScreenHeight() *0.95f, paint);
        _canvas.drawText("Hour: " + (int)GameDataManager.Instance.getHourOfTheDay(), 0, 80, paint);
        _canvas.drawText("Money: " + (int)CharacterData.Instance.getMoney(), GameDataManager.Instance.GetScreenWidth() * 0.5f, 80, paint);
        _canvas.drawText(""+(int)CharacterData.Instance.getInteracting(), (int)CharacterData.Instance.getPosition().x, (int)CharacterData.Instance.getPosition().y, paint);
//        paint.setARGB(255,255,255,0);
//        paint.setTextSize(50);
//        _canvas.drawText("Energy:" , GameDataManager.Instance.GetScreenWidth()* 0.85f,GameDataManager.Instance.GetScreenHeight()* 0.1f ,paint);
//        Texts text = textList.get(0);
//        paint.setARGB(text.color.alpha, text.color.x,text.color.y,text.color.z);
//        paint.setTextSize(70);
//        _canvas.drawText(text.text,text.pos.x,text.pos.y,paint);
//

        if(!textList.isEmpty()) {
            for (int i = 0; i < textList.size(); ++i) {
                Texts text = textList.get(i);
                text.setRiseEffect(text.riseEffect);
                paint.setARGB(text.color.alpha, text.color.x, text.color.y, text.color.z);
                paint.setStrokeWidth(200);
                paint.setTextSize(70);
                _canvas.drawText(text.text ,text.pos.x, text.pos.y, paint);
                text = null; //delete
            }
        }
    }

    public static RenderTextEntity Create()
    {
        RenderTextEntity result = new RenderTextEntity();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.RENDERTEXT_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
}
