package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

import java.util.LinkedList;
import java.util.Queue;

public class Entity_Character implements EntityBase, Collidable {
    //Init any variables here
    public final static Entity_Character Instance = new Entity_Character(0, 0);


    protected static final String TAG = null;
    private boolean isInit = false;
    private Bitmap bmp = null;
    private Sprite spriteSheet = null;
    private Sprite spriteUp = null;
    private Sprite spriteLeft = null;
    private Sprite spriteDown = null;
    private Sprite spriteRight = null;
    private Bitmap walkbmp = null;

    boolean isDone = false;
    boolean sprinting = false;
    float staminaBar = 100;

    public Vector3 pos;
    public Vector3 target;

    final float MAX_STAMINA = 100;
    final float MIN_STAMINA = 0;
    final float defaultMoveSpeed = 300f;
    final float boostedMoveSpeed = 500f;
    static boolean buffer = false;
    float bufferTimer = 0;

    public float moveSpeed = defaultMoveSpeed; // big value for big bois
    Queue<Vector3> vector3Queue = new LinkedList<Vector3>();

    public boolean interacting = false;

    enum MOVEMENT {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    static public Queue<MOVEMENT> movementQueue = new LinkedList<MOVEMENT>();
    MOVEMENT lastMovement;
    String direction;

    //Access the vertical and height of index
    int verticalHeight, horizontalHeight;

    //I guess i dont want to repeat pathfinding
    private int targetGridX = -1;
    private int targetGridY = -1;
    private int gridX, gridY;
    static float rubbishDrop = 0;

    private Entity_Character() {

    }

    private Entity_Character(float xPosition, float yPosition) {
        pos = new Vector3(xPosition, yPosition);
        target = pos; // init target to just be yourself first..
    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public boolean IsInit() {
        return isInit;
    }

    @Override
    public void Init(SurfaceView _view) {

        // Define anything you need to use here
        spriteSheet = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.megamanv2), 1, 7, 14);
        spriteUp = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.charup), 1, 3, 14);
        spriteDown = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.chardown), 1, 3, 14);
        spriteRight = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.charright), 1, 3, 14);
        spriteLeft = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.charleft), 1, 3, 14);

        walkbmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.crystalball);
        walkbmp = Bitmap.createScaledBitmap(walkbmp, GameDataManager.Instance.GetGridSize(), GameDataManager.Instance.GetGridSize(), true);

        AudioManager.Instance.Init(_view);
    }

    static float timer = 1.f;

    @Override
    public void Update(float _dt) {
//        System.out.println("GridXClicked: "+ targetGridX + " GridYClicked: " + targetGridY);
//        System.out.println("GridX: "+ gridX + " GridY: " + gridY);
        if (!interacting) {
            if (!sprinting) {
                // Update based on dt
                if (TouchManager.Instance.IsMove()) {
                    target = pos;
                    //check collision here
                    float imgRadius = spriteSheet.GetHeight() * 0.5f;
                    //            if(Collision.SphereToSphere(TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY(), 0.0f, xPos,yPos,imgRadius))
                    //            {
                    //                SetIsDone(true);
                    //            }

                    DoPathfinding();
                }
                if (!buffer) {
                    //Recover stamina
                    if (staminaBar < MAX_STAMINA) {
                        staminaBar += 10 * _dt;
                        if (staminaBar > MAX_STAMINA) {
                            staminaBar = MAX_STAMINA;
                        }
                    }
                    moveSpeed = defaultMoveSpeed;
                } else {
                    timer += 10 * _dt;
                    if (timer >= 2) {
                        Texts text = new Texts("Gaining stamina...", 2, new Vector3(pos.x, pos.y + GetRadius()), true);
                        RenderTextEntity.textList.add(text);
                        text = null;
                        timer = 0;
                    }
                }

            } else {
                if (!buffer) {
                    if (staminaBar > MIN_STAMINA) {
                        staminaBar -= 25 * _dt;
                        moveSpeed = boostedMoveSpeed;
                        if (staminaBar <= MIN_STAMINA) {
                            staminaBar = MIN_STAMINA;
                            moveSpeed = defaultMoveSpeed;
                            Texts text = new Texts("Out of stamina!", 2, new Vector3(pos.x, pos.y + GetRadius()), true);
                            RenderTextEntity.textList.add(text);
                            text = null;
                            buffer = true;
                        }
                    }
                }
                sprinting = false;
            }
            //Stamina recharge
            if (buffer) {
                bufferTimer += _dt;
                if (bufferTimer >= 2) {
                    bufferTimer = 0;
                    buffer = false;
                }
                System.out.println("Buffering: ");
            }
            //Assign the stamina into the data of char

            //Movement section
            Vector3 dir = Vector3.Sub(target, pos);
            if (dir.Length() < moveSpeed * _dt) {
                //System.out.println("Reached");
                pos = target;
                if (!vector3Queue.isEmpty()) {
                    target = vector3Queue.peek();
                    vector3Queue.remove();
                    if (!movementQueue.isEmpty()) {
                        if (movementQueue.size() == 1) // last element
                        {
                            lastMovement = movementQueue.peek();
                        }
                        movementQueue.remove();
                    }
                }
            } else {
                try {
                    dir.Normalize();
                    dir.Mult(moveSpeed * _dt);
                    pos.Add(dir);
                    if (!sprinting) {
                        AudioManager.Instance.PlayAudio(R.raw.steps, 1, 6);
                    } else {
                        AudioManager.Instance.PlayAudio(R.raw.steps, 1, 12);
                    }
//                System.out.println(pos.toString());
                } catch (Exception e) {

                }
            }
            spriteSheet.Update(_dt);

            if (!movementQueue.isEmpty()) {
                switch (movementQueue.peek()) {
                    case DOWN:
                        spriteDown.Update(_dt);
                        break;
                    case UP:
                        spriteUp.Update(_dt);
                        break;
                    case LEFT:
                        spriteLeft.Update(_dt);
                        ;
                        break;
                    case RIGHT:
                        spriteRight.Update(_dt);
                        break;
                }
            }
        }

        Entity_InteractButton.Instance.render = CharacterData.Instance.isNearFire;
        //System.out.println(CharacterData.Instance.isNearFire);
        CharacterData.Instance.stamina = staminaBar;
        CharacterData.Instance.position = pos;
        CharacterData.Instance.isNearFire = false;
        interacting = false;

    }

    @Override
    public void Render(Canvas _canvas) {
//        // Render anything
//        if(movementQueue.peek() == MOVEMENT.DOWN) {
//            spriteSheet.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
//        }
        if (!movementQueue.isEmpty()) {
            switch (movementQueue.peek()) {
                case DOWN:
                    spriteDown.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                    CharacterData.Instance.setDirection("Down");
                    break;
                case UP:
                    spriteUp.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                    CharacterData.Instance.setDirection("Up");
                    break;
                case LEFT:
                    spriteLeft.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                    CharacterData.Instance.setDirection("Left");
                    break;
                case RIGHT:
                    spriteRight.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                    CharacterData.Instance.setDirection("Right");
                    break;
            }
        } else {
            if (lastMovement != null) {
                switch (lastMovement) {
                    case DOWN:
                        spriteDown.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                        CharacterData.Instance.setDirection("Down");

                        break;
                    case UP:
                        spriteUp.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                        CharacterData.Instance.setDirection("Up");
                        break;
                    case LEFT:
                        spriteLeft.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                        CharacterData.Instance.setDirection("Left");
                        break;
                    case RIGHT:
                        spriteRight.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                        CharacterData.Instance.setDirection("Right");
                        break;
                }
            } else {
                spriteDown.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
            }
        }


        Queue<Vector3> tempQueue =  new LinkedList<Vector3>(vector3Queue);

        while(!tempQueue.isEmpty())
        {
            Vector3 pos = tempQueue.peek();
            _canvas.drawBitmap(walkbmp,  (int) pos.x,GameDataManager.Instance.GetGridOffset() + (int) pos.y, null); //left = x, top = y, counting from top
            tempQueue.remove();
        }



    }

    public static Entity_Character Create() {
        Entity_Character result = new Entity_Character();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    public static Entity_Character Create(float xPosition, float yPosition) {
        // System.out.println("Why do you not work");
        Entity_Character result = new Entity_Character(xPosition, yPosition);
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public String GetType() {
        return "Entity_Character";
    }

    @Override
    public float GetPosX() {
        return pos.x;
    }

    @Override
    public float GetPosY() {
        return pos.y;
    }

    @Override
    public float GetRadius() {
        return spriteDown.GetHeight() * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other) {
        if (_other.GetType() == "Entity_Television") {
            if (_other.GetRadius() != 0 && CharacterData.Instance.holdingFire == false) {
                interacting = true;
            }

        } else if (_other.GetType() == "Entity_Fire") {
            if(CharacterData.Instance.holdingFire) {
                interacting = true;
            }
        } else if (_other.GetType() == "Entity_Water") {
            if(CharacterData.Instance.holdingFire == false) {
                interacting = true;
            }
        } else if (_other.GetType() == "Entity_FireExtinguisher") {
            CharacterData.Instance.setNearFire(true);
        }

    }

    @Override
    public void TouchHit() {
        sprinting = true;
    }

    @Override
    public boolean GetAttending() {
        return false;
    }

    void DoPathfinding() {
        int currentGridX = (int) Math.floor(pos.x / GameDataManager.Instance.GetGridSize());
        int currentGridY = (int) Math.floor(pos.y / GameDataManager.Instance.GetGridSize());
        gridX = (int) Math.floor(TouchManager.Instance.GetPosX() / GameDataManager.Instance.GetGridSize());
        gridY = (int) Math.floor(TouchManager.Instance.GetPosY() / GameDataManager.Instance.GetGridSize());
        // pos.x = GameDataManager.Instance.GetGridOffset() + (gridX *GameDataManager.Instance.GetGridSize());
        // pos.y = GameDataManager.Instance.GetGridOffset() + (gridY ) *GameDataManager.Instance.GetGridSize();

        verticalHeight = DataManager.Instance.ReturnData(0).heightOfGrid;
        horizontalHeight = DataManager.Instance.ReturnData(0).numberOfGrid;

        //hardcode 2 first
        //If its in the radius
        if (gridX >= 2 && gridX < horizontalHeight - 2 && gridY >= 2 && gridY < verticalHeight - 2) {
            if (Grid.Instance.grid[gridX][gridY].walkable) {

                while (!vector3Queue.isEmpty()) {
                    vector3Queue.remove(); // reset vector queue // cant use clear cause abstract class
                }
                Pathfinding.Instance.FindPath(new Node(false, null, currentGridX, currentGridY), new Node(false, null, gridX, gridY));
                if (Grid.Instance.path != null) {
                    for (Node var : Grid.Instance.path) {
                        vector3Queue.add(new Vector3(var.gridX * GameDataManager.Instance.GetGridSize(),
                                var.gridY * GameDataManager.Instance.GetGridSize(),
                                0));
                    }
                    UpdateSprite();
                }
                //After that i clear the grid just to be sure
                //Grid.Instance.path.clear();
            } else {
                while (!vector3Queue.isEmpty()) {
                    vector3Queue.remove(); // reset vector queue // cant use claer cause abstract class
                }
                if (vector3Queue.isEmpty()) {
                    for (Node neighbours : Grid.Instance.GetNeighbours(Grid.Instance.grid[gridX][gridY])
                            ) {
                        // System.out.println("Point clicked was: X: " + gridX + "Y:" + gridY + "Neighbour: X:" + neighbours.gridX + "Y:" + neighbours.gridY);
                        if (neighbours.walkable) {
                            Pathfinding.Instance.FindPath(new Node(false, null, currentGridX, currentGridY), new Node(false, null, neighbours.gridX, neighbours.gridY));
                            //System.out.println("Path found in neighbour::");
                            if (Grid.Instance.path != null) {
                                for (Node var : Grid.Instance.path) {
                                    //System.out.println(var.gridX + "," + var.gridY);
                                    vector3Queue.add(new Vector3(var.gridX * GameDataManager.Instance.GetGridSize(), var.gridY * GameDataManager.Instance.GetGridSize(), 0));
                                }
                                UpdateSprite();
                            }
                            break;
                        }
                    }
                }
                //System.out.println("Out of range");
            }
        }
    }

    void UpdateSprite() {
        //Clear the queue.
        while (!movementQueue.isEmpty()) {
            movementQueue.remove(); // reset vector queue // cant use clear cause abstract class
        }

        for (int i = 1; i < Grid.Instance.path.size(); ++i) {
            Node previous, current;
            previous = Grid.Instance.path.get(i - 1);
            current = Grid.Instance.path.get(i);

            //Check right
            if (current.gridX > previous.gridX && current.gridY == previous.gridY) {
                movementQueue.add(MOVEMENT.RIGHT);
            }
            //Diagonal top right and bottom right
            else if (current.gridX > previous.gridX && current.gridY > previous.gridY) {
                movementQueue.add(MOVEMENT.RIGHT);
            }
            //Check left
            else if (current.gridX < previous.gridX && current.gridY == previous.gridY) {
                movementQueue.add(MOVEMENT.LEFT);
            }
            //Diagonal top left and bottom left
            else if (current.gridX < previous.gridX && current.gridY < previous.gridY) {
                movementQueue.add(MOVEMENT.LEFT);
            }
            //Check up
            else if (current.gridY > previous.gridY && current.gridX == previous.gridX) {
                movementQueue.add(MOVEMENT.DOWN);
            }
            //Check bottom
            else if (current.gridY < previous.gridY && current.gridX == previous.gridX) {
                movementQueue.add(MOVEMENT.UP);
            }

            previous = null; //save memory?
            current = null; //save memory?
        }
    }

    @Override
    public int GetRenderLayer() {
        return LayerConstants.GAMEOBJECTS_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayer) {
        return;
    }


}

