package com.sidm.project1;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LevelManager {

    public final static LevelManager Instance = new LevelManager();
    private LevelManager() {
    }

    Map<String,Vector3> locationMap = new HashMap<String,Vector3>();
    Map<String, Boolean> booleanMap = new HashMap<String,Boolean>();

    void AddInteractable(String string, Vector3 vector3)
    {
        locationMap.put(string,vector3);
        booleanMap.put(string, false);
    }

    void RemoveInteractable(String string)
    {
        if(locationMap.containsKey(string)){
            locationMap.remove(string);
            System.out.println("Element" + string + "removed!");
        }
        else {
            System.out.println("No such element in locationMap");
        }

        if(booleanMap.containsKey(string)){
            booleanMap.remove(string);
            System.out.println("Element" + string + "removed!");
        }
        else {
            System.out.println("No such element in booleanMap");
        }

    }

    void ClearEntireMap()
    {
        booleanMap.clear();
        locationMap.clear();
    }

    Vector3 GetInteractblePos(String string)
    {
        return locationMap.get(string);
    }

    Boolean GetInteractable(String string)
    {
        if(booleanMap.containsKey(string)) {
            return booleanMap.get(string);
        }
        else
        {
            return null;
        }
    }


}
