package com.sidm.project1;

import android.app.backup.FileBackupHelper;
import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class InternalDataManager {

    final static String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/data/";
    final static String tag = FileBackupHelper.class.getName();

    public static String ReadFile(Context context, String filePath)
    {
        String line = null;
        try
        {
            FileInputStream fileInputStream = new FileInputStream(new File(path + filePath));
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();

            //Reading line by line
            while((line = bufferedReader.readLine()) != null)
            {
                stringBuilder.append(line);
            }
            fileInputStream.close();

            line = stringBuilder.toString();
            bufferedReader.close();

        } catch(IOException e)
        {
            //Need???
            Log.d(tag,e.getMessage());
        }
        return  line;
    }


    //Writing to File
    public static boolean saveData(String data, String dataPath, boolean isAppend) {
        try {
            new File(path).mkdir();
            File file = new File(path + dataPath);

            //Make the file
            if (!file.exists()) {
                file.createNewFile();
            }

            FileOutputStream fileOutputStream = new FileOutputStream(file, isAppend);
            fileOutputStream.write(data.getBytes());

            return true;
        } catch (IOException e) {
            Log.d(tag, e.getMessage());
        }
        return false;
    }
}
