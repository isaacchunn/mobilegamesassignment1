package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;

import org.w3c.dom.Entity;

public class Entity_Obstacle implements EntityBase, Collidable {

    private boolean isInit = false;
    private Bitmap bmp = null;
    private Sprite spriteSheet = null;
    boolean isDone = false;
    private  float xPos, yPos;
    int ScreenWidth=0,ScreenHeight=0;
    DisplayMetrics metrics =  new DisplayMetrics();



    public Vibrator m_vibrator;

    Entity_Obstacle(){

    }

    Entity_Obstacle(float xPosition, float yPosition)
    {
        xPos = xPosition;
        yPos = yPosition;
    }
    @Override
    public boolean GetAttending()
    {
        return false;
    }
    @Override
    public void Init(SurfaceView _view) {

        // Define anything you need to use here
        spriteSheet = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.megamanv3), 1,4,16);
        //spriteSheet = new Sprite(ResourceManager.Instance.GetBitmap(R.drawable.megamanv3), 1,4,16);
        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.ship2_1); //other images

//
//        xPos =(float)(ScreenWidth*0.2);
//        yPos =(float)(ScreenHeight*0.5);
        m_vibrator = (Vibrator) _view.getContext().getSystemService(_view.getContext().VIBRATOR_SERVICE);

    }

    @Override
    public void Update(float _dt) {
        // Update based on dt

        if(TouchManager.Instance.IsMove())
        {
            //check collision here
            float imgRadius = spriteSheet.GetHeight() *0.5f;
            if(Collision.SphereToSphere(TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY(), 0.0f, xPos,yPos,imgRadius))
            {
                SetIsDone(true);
                startVibrate();
            }
/*
            xPos = TouchManager.Instance.GetPosX();
            yPos = TouchManager.Instance.GetPosY();
*/
        }
        spriteSheet.Update(_dt);

    }

    @Override
    public void Render(Canvas _canvas) {

        // Render anything
        spriteSheet.Render(_canvas,(int)xPos,(int)yPos);
    }

    public static Entity_Obstacle Create()
    {
        Entity_Obstacle result = new Entity_Obstacle();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    public static Entity_Obstacle Create(float xPosition, float yPosition)
    {
        Entity_Obstacle result = new Entity_Obstacle(xPosition,yPosition);
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    public static Entity_Obstacle Create(int _layer)
    {
        Entity_Obstacle result = Create();
        result.SetRenderLayer(_layer);
        return result;
    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }
    @Override
    public boolean IsInit() {
        return isInit;
    }

    @Override
    public String GetType() {
        return "Entity_Obstacle";
    }

    @Override
    public float GetPosX() {
        return xPos;
    }

    @Override
    public float GetPosY() {
        return yPos;
    }

    @Override
    public float GetRadius()
    {
        return spriteSheet.GetHeight()*0.5f;
    }
    @Override
    public void OnHit(Collidable _other)
    {
        //if(_other.GetType() == "Entity_Character")
        //{
             Texts text = new Texts("Collided", 2, new Vector3(xPos,yPos + GetRadius()),true);
            RenderTextEntity.textList.add(text);
            SetIsDone(true);
            text = null;
        //}
    }
    @Override
    public void TouchHit()
    {

    }
    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.GAMEOBJECTS_LAYER;
    }
    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
    public void startVibrate()
    {

        //Amplitude is an int value. It's the strength of the vibration
        //this must be avalue between 1 and 255, or DEFAULT_AMPLITUDE which is -1
        //VibrationEffect effect = VibrationEffect.createOneShot(1000,VibrationEffect,DEFAULT_AMPLITURE);

        if(Build.VERSION.SDK_INT >=26)
        {
            m_vibrator.vibrate(VibrationEffect.createOneShot(150,10));
        }
        else {
            //pattern[0] time to wait before vibrator is on
            //pattern [1] time to keep vibrator on
            //pattern[2] time till vibration is off ot till vibrator is on
            long pattern[] = {0,50,0};
            //1000 = Vibrate 1 sec

            m_vibrator.vibrate(pattern, -1);
            Log.v("TAG", "Test if vibration had occured");
        }
    }

    public void stopVibrate()
    {
        m_vibrator.cancel();
    }
}
