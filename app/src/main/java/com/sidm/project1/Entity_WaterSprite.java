package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.SurfaceView;

import java.util.Random;

public class Entity_WaterSprite implements EntityBase, Collidable  {

    public Vector3 pos;
    boolean isDone = false;
    private boolean isInit = false;

    private Sprite spriteSheet;
    private Bitmap bitmap;

    public Entity_WaterSprite(float xPosition, float yPosition)
    {
        pos = new Vector3(xPosition,yPosition);
    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public boolean IsInit() {
        return isInit;
    }

    @Override
    public void Init(SurfaceView _view) {
        // Define anything you need to use here
        bitmap = BitmapFactory.decodeResource(_view.getResources(), R.drawable.waterspray);
        bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() /2, bitmap.getHeight() /2, true);
        spriteSheet = new Sprite(bitmap, 1,7,16);


    }

    @Override
    public void Update(float _dt) {
        spriteSheet.Update(_dt);
        spriteSheet.setRepeat(false);

        if(spriteSheet.getDone())
        {
            SetIsDone(true);
        }
    }

    @Override
    public void Render(Canvas _canvas) {

        Matrix transform = new Matrix();

        transform.postTranslate(-spriteSheet.GetWidth() *  0.5f, -spriteSheet.GetHeight() * 0.5f);
        transform.postScale(GameDataManager.Instance.GetGridSize(), GameDataManager.Instance.GetGridSize());
        transform.postTranslate(pos.x, pos.y);

        spriteSheet.Render(_canvas,(int)pos.x,(int)pos.y);

    }
    @Override
    public String GetType() {
        return "Entity_WaterSprite";
    }

    @Override
    public float GetPosX() {
        return pos.x;
    }

    @Override
    public float GetPosY() {
        return pos.y;
    }

    @Override
    public float GetRadius() {
        return spriteSheet.GetWidth() * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other) {

    }

    @Override
    public void TouchHit() {

    }
    @Override
    public boolean GetAttending()
    {
        return false;
    }

    public static Entity_WaterSprite Create(float xPosition, float yPosition) {
        Entity_WaterSprite result = new Entity_WaterSprite(xPosition, yPosition);
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    public static Entity_WaterSprite CreateIntoList(float xPosition, float yPosition) {
        Entity_WaterSprite result = new Entity_WaterSprite(xPosition, yPosition);
        EntityManager.Instance.AddEntityIntoList(result);

        return result;
    }

    public static Entity_WaterSprite CreateIntoListRelative() {
        Vector3 newPos = new Vector3(CharacterData.Instance.position.x,CharacterData.Instance.position.y,CharacterData.Instance.position.z);

        if(CharacterData.Instance.getDirection() != null) {
            switch (CharacterData.Instance.getDirection()) {
                case"Up":
                    newPos.y -= GameDataManager.Instance.GetGridSize();
                    break;
                case "Down":
                    newPos.y += GameDataManager.Instance.GetGridSize();
                    break;
                case "Left":
                    newPos.x -= GameDataManager.Instance.GetGridSize();
                    break;
                case "Right":
                    newPos.x += GameDataManager.Instance.GetGridSize();
                    break;
            }
        }

        Entity_WaterSprite result = new Entity_WaterSprite(newPos.x, newPos.y);
        EntityManager.Instance.AddEntityIntoList(result);

        return result;
    }


    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.GAMEOBJECTS_LAYER;
    }
    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
}

