package com.sidm.project1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//An actually useless class for debugging so i can access the list and combined string at any time
public class Data {

    //Constructor
    public ArrayList dataList = new ArrayList();
    public String formattedData = "";
    public int mapInstance = -1;
    public int numberOfGrid;
    public int heightOfGrid;
    //What we're using
    public int[] intArray;
    public Map<Integer,Boolean> walkableMap = new HashMap<Integer,Boolean>();

    enum TILES{
        WALLS,
        EMPTY,
        SPRITE,
        DEFAULT
    }

    //The tiles array for each map
    public  TILES[] tilesArray;

    Data(ArrayList list, String formattedData, int numberOfGrid, int heightOfGrid)
    {
        dataList = list;
        this.formattedData = formattedData;

        //Handle the data;
        intArray = new int[dataList.size()];
        for(int i =0;i < dataList.size() ; ++i)
        {
            intArray[i] = (int)list.get(i);
        }

        UpdateTileArray();
        UpdateWalkableAreas();
        this.numberOfGrid = numberOfGrid;
        this.heightOfGrid = heightOfGrid;
    }

    void UpdateTileArray()
    {

        tilesArray = new  TILES[intArray.length];
        for(int i =0; i < intArray.length; ++i)
        {
            switch(intArray[i])
            {
                case -1:
                {
                    tilesArray[i] = TILES.EMPTY;
                    break;
                }
                case 1:
                {
                    tilesArray[i] = TILES.WALLS;
                    break;
                }
                case 2:
                {
                    tilesArray[i] = TILES.SPRITE;
                    break;
                }
                case 0: // for DFS
                {
                    tilesArray[i] = TILES.DEFAULT;
                    break;
                }

            }
        }
    }

    void UpdateWalkableAreas()
    {

        //Reset the thing
        for(int i =0; i < intArray.length; ++i) {
        walkableMap.put(i,true);
        }


        for(int i =0; i < intArray.length; ++i) {
            switch(intArray[i]) {
                case -1:
                {
                    walkableMap.put(i,true);
                    break;
            }
                default:
                {
                    walkableMap.put(i,false);
                    break;
                }
        }
    }
    }
}
