package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

//use collision code for button ecks dee
public class Entity_Button implements EntityBase, Collidable {

    public Vector3 pos;
    boolean isDone = false;
    private boolean isInit = false;
    private Bitmap sprintButton = null;
    enum BUTTON_TYPE
    {
        CIRCLE,
        RECT,
    }
    Entity_Button(float xPosition, float yPosition)
    {
        pos = new Vector3(xPosition,yPosition);

    }
    @Override
    public boolean GetAttending()
    {
        return false;
    }
    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }
    @Override
    public boolean IsInit() {
        return isInit;
    }
    @Override
    public void Init(SurfaceView _view) {
        sprintButton = BitmapFactory.decodeResource(_view.getResources(), R.drawable.sprint); //other images
        sprintButton = Bitmap.createScaledBitmap(sprintButton, GameDataManager.Instance.GetGridSize() * 3, GameDataManager.Instance.GetGridSize() * 3, true);
    }

    @Override
    public void Update(float _dt) {
        // Update based on dt
    }

    @Override
    public void Render(Canvas _canvas) {
        if(!CharacterData.Instance.holdingFire) {
            _canvas.drawBitmap(sprintButton, pos.x, pos.y, null); //left = x, top = y, counting from top left corner(0,0)
        }
    }

    public static Entity_Button Create(float xPosition, float yPosition)
    {
        Entity_Button result = new Entity_Button(xPosition,yPosition);
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public String GetType() {
        return "Entity_Button";
    }

    @Override
    public float GetPosX() {
        return pos.x;
    }

    @Override
    public float GetPosY() {
        return pos.y;
    }

    @Override
    public float GetRadius()
    {
        return sprintButton.getHeight()*0.5f;

    }
    @Override
    public void OnHit(Collidable _other)
    {
        //if(_other.GetType() == "Entity_Character")
        //{
        //    SetIsDone(true);
        //}
    }
    @Override
    public void TouchHit()
    {
        //In SampleGame, IsPaused = false;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.UI_LAYER;
    }
    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
}
