package com.sidm.project1;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class PauseConfirmDialogFragment extends DialogFragment{

    public static boolean IsShown = false; //to indicate false if builder is triggered

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        IsShown = true;

        //Use the builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Confirm Pause?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //User triggered pause
                GameSystem.Instance.SetIsPaused(!GameSystem.Instance.GetIsPaused());
                IsShown = false;
            }
        })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //User cancelled the pause
                IsShown = false;
            }
        });

        return builder.create();
    }
}
