package com.sidm.project1;

public class Color {
    public int x;
    public int y;
    public int z;
    public int alpha;

    Color()
    {
        this.x = 255;
        this.y= 255;
       this. z = 255;
       this. alpha = 255;
    }

    Color(int x, int y, int z)
    {
       this.x = x;
        this.y= y;
        this.z = z;
        this.alpha = 255;
    }

    Color(int x, int y, int z, int alpha)
    {
        this.x = x;
        this.y= y;
        this.z = z;
        this.alpha = alpha;
    }

}
