package com.sidm.project1;

import android.graphics.Canvas;
import android.view.SurfaceView;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class EntityManager {
    public final static EntityManager Instance = new EntityManager();
    public LinkedList<EntityBase> entityList = new LinkedList<EntityBase>();
    private SurfaceView view = null;

    public LinkedList<EntityBase>addList = new LinkedList<>();

    private EntityManager() {
    }

    public void Init(SurfaceView _view) {
        view = _view;
    }

    public void Update(float _dt) {

        LinkedList<EntityBase> removalList = new LinkedList<EntityBase>();

        if(addList != null) {
            for (EntityBase entity : addList) {
                entityList.add(entity);
            }
            addList.clear();
        }
        // Update all
        for (EntityBase currEntity : entityList) {
            currEntity.Update(_dt);

            // Check if need to clean up
            if (currEntity.IsDone())
                removalList.add(currEntity);
        }

        // Remove all entities that are done
        for (EntityBase currEntity : removalList)
            entityList.remove(currEntity);

        removalList.clear(); // Clean up of removal list

        // Collision Check
        for (int i = 0; i < entityList.size(); ++i) {
            EntityBase currEntity = entityList.get(i);

            if (currEntity instanceof Collidable) {
                Collidable first = (Collidable) currEntity;

                for (int j = i + 1; j < entityList.size(); ++j) {
                    EntityBase otherEntity = entityList.get(j);

                    if (otherEntity instanceof Collidable) {
                        Collidable second = (Collidable) otherEntity;

                        if(TouchManager.Instance.HasTouch()) {
                            if(second.GetType() == "Entity_Button")
                            {
                                if (Collision.SphereToSphere(second.GetPosX()+ second.GetRadius(), second.GetPosY()+second.GetRadius(), second.GetRadius(), TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY(), 5)) {

                                    if(first.GetType()== "Entity_Character")
                                    {
                                        //found character;
                                        if(CharacterData.Instance.holdingFire == false) {
                                            first.TouchHit();
                                        }
                                    }
                                }
                            }
                        }

                        if (Collision.SphereToSphere(first.GetPosX(), first.GetPosY(), first.GetRadius(), second.GetPosX(), second.GetPosY(), second.GetRadius())) {
                            first.OnHit(second);
                            second.OnHit(first);
                        }

                    }
                }

                if(TouchManager.Instance.IsDown())
                {
                    if (Collision.SphereToSphere(first.GetPosX() + first.GetRadius(), first.GetPosY() + first.GetRadius(), first.GetRadius(), TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY(), 5)) {

                        if(((Collidable) currEntity).GetType() == "Entity_PauseButton") {

                            ((Collidable) currEntity).TouchHit();
                        }

                        else if (((Collidable) currEntity).GetType() == "Entity_InteractButton") {

                            ((Collidable) currEntity).TouchHit();
                        }

                        else if (((Collidable) currEntity).GetType() == "Entity_SprayButton") {

                            ((Collidable) currEntity).TouchHit();
                        }
                    }
                }
                if(TouchManager.Instance.HasTouch()) {
                    if (Collision.SphereToSphere(first.GetPosX(), first.GetPosY(), first.GetRadius(), TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY(), 5)) {
                        if(((Collidable) currEntity).GetType() == "Entity_Obstacle") {
                            //currEntity.SetIsDone(true);
                            continue;
                        }
                        if(((Collidable) currEntity).GetType() == "Entity_Button") {
                            //currEntity.SetIsDone(true);
                            }


                        }

                    if (Collision.SphereToSphere(first.GetPosX() + first.GetRadius(), first.GetPosY() + first.GetRadius(), first.GetRadius(), TouchManager.Instance.GetPosX(), TouchManager.Instance.GetPosY(), 5)) {

                    }
                       /* if(((Collidable) currEntity).GetType() == "Entity_PauseButton") {

                            ((Collidable) currEntity).TouchHit();
                        }*/
                       // first.OnHit(first);
                            //  second.OnHit(first);
                    }

                }


            //    Check if need to clean up
            if (currEntity.IsDone())
                removalList.add(currEntity);

        }

        // Remove all entities that are done
        for (EntityBase currEntity : removalList)
            entityList.remove(currEntity);


    }

    public void Render(Canvas _canvas) {

        //Week 9
        //We will use the new "rendering layer" to sort the render order
        Collections.sort(entityList, new Comparator<EntityBase>() {
            @Override
            public int compare(EntityBase o1, EntityBase o2) {
                return o1.GetRenderLayer() - o2.GetRenderLayer();
            }
        });

        //Week 4-5
        for (EntityBase currEntity : entityList) {
            currEntity.Render(_canvas);
        }

    }

    public void AddEntity(EntityBase _newEntity) {
        _newEntity.Init(view);
        entityList.add(_newEntity);
    }
    public void AddEntityIntoList(EntityBase _newEntity) {
        _newEntity.Init(view);
        addList.add(_newEntity);

    }

/* n
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int x = (int)event.getX();
        int y = (int)event.getY();

        TouchManager.Instance.Update(x,y,event.getAction());

        return false;
    }
*/
}

