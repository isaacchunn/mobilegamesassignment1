package com.sidm.project1;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.view.SurfaceView;

public class GameSystem {

    public final static GameSystem Instance = new GameSystem();

    public static final String SHARED_PREF_ID = "GameSaveFile";
    //Game stuff
    private boolean isPaused = false;

    SharedPreferences sharedPref = null;
    SharedPreferences.Editor editor = null;

    //Singleton pattern  : blocks others from creating
    private GameSystem()
    {

    }

    public void Update(float _deltaTime)
    {

        StateManager.Instance.Update(_deltaTime);
    }

    public void Render(Canvas _canvas)
    {
        StateManager.Instance.Render(_canvas);
    }

    public void Init(SurfaceView _view)
    {
        //Get our shared preferences (Save file)
        //sharedPref = GamePage.Instance.getSharedPreferences(SHARED_PREF_ID,0);

        //We will add all of our states into the state manager here
        StateManager.Instance.AddState(new SampleGame());
        StateManager.Instance.AddState(new MainGameState());



        //StateManager.Instance.AddState(new IntroState()); //we dont have a introstate
    }

    public void SetIsPaused(boolean _newIsPaused)
    {
        isPaused = _newIsPaused;
    }

    public boolean GetIsPaused()
    {
        return isPaused;
    }

    public void SaveEditEnd()
    {
        //check if has editor
        if(editor == null)
        {
            return;
        }

        editor.commit();
        editor = null; // safety to ensure other functions will fail once commit done
    }

    public void SetIntInSave(String _key, int _value)
    {
        if(editor == null)
        {
            return;
        }
        editor.putInt(_key,_value);
    }

    public int GetIntFromSave(String _key)
    {
        if(editor == null)
        {
            return 0;
        }

        //editor.
        return 0;
    }
}
