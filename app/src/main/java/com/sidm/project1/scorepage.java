package com.sidm.project1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.Login;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class scorepage extends Activity implements View.OnClickListener{
    public CallbackManager callbackManager;
    public LoginManager loginManager;

    private static final String EMAIL = "email";
    private ShareDialog shareDialog;
    public LoginButton btn_fbLogin;
    public Button btn_share;
    //ShareDialog shareDialog
    ProfilePictureView profile_pic;
    static int PICK_IMAGE_REQUEST = 1;
    List<String> PERMISSIONS = Arrays.asList("publish_actions");

    int highscore;
    int highscore2;
    String playername;
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        //week 14 - init for fb
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.scorepage);


        TextView scoreText;
        scoreText = (TextView)findViewById(R.id.scoreText);


        Typeface myfont;
        myfont = Typeface.createFromAsset(getAssets(), "fonts/Gemcut.otf");

        highscore = DataSaver.Instance.GetIntFromSave("highscorelevel1");
        highscore2 = DataSaver.Instance.GetIntFromSave("highscorelevel2");

        playername = "Salted Egg Yolk";

        scoreText.setText(String.format(playername + "'s highscore for Level 1 is "+ highscore + " and the highscore for Level 2 is " + highscore2));
        //scoreText.setText("abc");

        btn_fbLogin = (LoginButton) findViewById(R.id.fb_login_button);
        btn_fbLogin.setReadPermissions(Arrays.asList(EMAIL));
        btn_share = (Button)findViewById(R.id.btn_sharescore);
        btn_share.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();

        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                //AccessToken at = AccecssToken.getCurrentAccessToken();

                /*if(currentAccessToken == null)
                {
                    profile_pic.setProfileId("");
                }
                else
                {
                    profile_pic.setProfileId(Profile.getCurrentProfile().getId());
                }*/


            }
        };

        accessTokenTracker.startTracking();

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

        LoginManager.getInstance().logInWithPublishPermissions(this,Arrays.asList("publish_actions"));
        //LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));

        loginManager = LoginManager.getInstance();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //profile_pic.setProfileId(Profile.getCurrentProfile().getId());

                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                //boolen isLoggedIn = accessToken !=null && !accessToken.isExpired();
                loginResult.getAccessToken().getUserId();

            }

            @Override
            public void onCancel() {
System.out.println("Login attempt canceled");
            }

            @Override
            public void onError(FacebookException error) {
            System.out.println("Login attempt failed");
            }
        });
    }

   public void shareScore()
   {
       Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

       SharePhoto photo = new SharePhoto.Builder()
               .setBitmap(image)
               .setCaption("Thank you for playing Happy Zappy Family. Your final score is " + highscore)
               .build();

       SharePhotoContent content = new SharePhotoContent.Builder()
               .addPhoto(photo)
               .build();

       ShareApi.share(content, null);
   }
    public void sharePhotos(View view) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

    }
   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data)
   {
        super.onActivityResult(requestCode,resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);

       if (resultCode == RESULT_OK) {
           shareScore();

           if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {

               Bitmap image = null;
               try {
                   image = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
               } catch (IOException e) {
                   e.printStackTrace();
               }

               SharePhoto photo = new SharePhoto.Builder()
                       .setBitmap(image)
                       .build();

               if (ShareDialog.canShow(SharePhotoContent.class))
               {
                   SharePhotoContent sharePhotoContent = new SharePhotoContent.Builder()
                           .addPhoto(photo)
                           .build();

                   ShareDialog share_Dialog = new ShareDialog(this);
                   share_Dialog.show(sharePhotoContent);
               }


           }

       }

   }

   protected  void onPause()
   {
       super.onPause();
   }

   @Override
   protected void onDestroy()
   {
       super.onDestroy();
   }

   @Override
    protected  void onStop()
   {
       super.onStop();
   }

    @Override
    public void onClick(View v) {
        if(v == btn_share)
        {
            shareScore();

        }
    }
}
