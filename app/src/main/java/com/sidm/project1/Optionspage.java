package com.sidm.project1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;

public class Optionspage extends Activity implements View.OnClickListener{

    private SeekBar volumeSeekbar;
    private Button btn_option_back;
    private Switch soundswitch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.optionspage);

        //setVolumeControlStream(AudioManager.STREAM_MUSIC);


        btn_option_back = (Button)findViewById(R.id.btn_option_back);
        btn_option_back.setOnClickListener(this);

        soundswitch = (Switch)findViewById(R.id.soundswitch);
        if(soundswitch.isChecked())
        {
            soundswitch.setChecked(true);
        }
        else
        {
            soundswitch.setChecked(false);
        }

        soundswitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    SoundManager.getInstance().play(getApplicationContext(), R.raw.bensound_littleidea, true);
                }
                else
                {
                    SoundManager.getInstance().stop();
                }
            }
        });
        initControls();
//        if (SoundManager.getInstance().paused != 0) {
//            SoundManager.getInstance().resume(this);
//        } else {
//            SoundManager.getInstance().play(this, R.raw.bensound_littleidea, true);
//        }

    }

    private void initControls () {
        try {
            volumeSeekbar = findViewById(R.id.seekBar);
            volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                   // audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,progress,0);
                    SoundManager.getInstance().player.setVolume(progress*0.01f,progress*0.01f);
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
        });

        }catch(Exception e)
        {

        }
    }

    @Override
    public void onClick(View v)
    {
        Intent intent = new Intent();
        if(v == btn_option_back)
        {
            intent.setClass(this,Mainmenu.class);
        }
        startActivity(intent);

    }


}
