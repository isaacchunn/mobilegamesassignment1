package com.sidm.project1;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.nfc.Tag;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceView;
import android.widget.TextView;

import org.w3c.dom.Entity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Random;

//Your game scene 1
public class SampleGame implements StateBase{

    //Write a singleton
    public final static SampleGame Instance = new SampleGame();

    //Define object name
    public Bitmap backgroundbmp; //pictures like png files are known as bit map
    //Scaled - scaled version based on Screenwidth & height for the image bmp

    //All bitmaps
    public Bitmap emptyBmp; //lenny face icon
    public Bitmap wallBmp; //just black
    public Bitmap exmark; //exclamation mark
    public Bitmap blackQuad; //black quad / just black
    public Bitmap scaledbackgroundbmp; //background bmp
    public Bitmap SemptyBmp;
    public Bitmap SwallBmp;
    public Bitmap scaledExmark;

    private Sprite spriteSheet; //smoke animation
    private Bitmap smokeBmp; //

    private boolean isFire  = false;

    public float elapsedTime = 0;

    int ScreenWidth, ScreenHeight; //screenwidth and height
    float xPos = 0.f, yPos = 0.f; //variable used for positioning of images

    //Containers.
    Data.TILES[] data; //The enum et each grid
    Map<Integer, Boolean> dataMap; // is the place walkable?
    ArrayList<Node> path; // the path the entity is supposed to take.

    private int gridSize, noOfGrid, gridOffset;
    Random seed = new Random();

    public boolean IsPaused = false;
    //DEBUG
    public boolean Debug = false;
    public SurfaceView view = null;
/*
    public Bitmap button_popup;
*/
    private boolean initialized = false;
    public SampleGame() {

    }


    @Override
    public String GetName()
    {
        return "SampleGame";
    }

    //basically init?
    @Override
    public void OnEnter(SurfaceView _View)
    {
/*
        EntityManager.Instance.entityList.clear();
        LevelManager.Instance.ClearEntireMap();

*/
/*
        if(SampleGame.Instance.initialized == false)
        {
            SampleGame.Instance.Init(_View);
            AudioManager.Instance.Init(_View);
        }
*/
    //btn_option_back = (Button)findViewById(R.id.btn_option_back);

        EntityManager.Instance.entityList.clear();
        LevelManager.Instance.ClearEntireMap();
        CharacterData.Instance.money = 200;

        Grid.Instance.CreateGrid(0);

        //empty the list first
        //Init the variables
        gridSize = GameDataManager.Instance.GetGridSize();
        noOfGrid = GameDataManager.Instance.GetNoOfGrid();
        gridOffset = GameDataManager.Instance.GetGridOffset();
        ScreenWidth = GameDataManager.Instance.GetScreenWidth();
        ScreenHeight = GameDataManager.Instance.GetScreenHeight();
        //Load Resources for e.g. Images

        backgroundbmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.tilemap);
        emptyBmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.icon);
        wallBmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.justblack);
        exmark= BitmapFactory.decodeResource(_View.getResources(), R.drawable.exmark);
/*
        button_popup = BitmapFactory.decodeResource(_View.getResources(), R.drawable.buttonxml);
*/
        //Scaled version of bmp which is background of game scene
        backgroundbmp = Bitmap.createScaledBitmap(backgroundbmp, ScreenWidth, ScreenHeight, true);
        exmark = Bitmap.createScaledBitmap(exmark, gridSize * 2, gridSize * 2, true);
        wallBmp = Bitmap.createScaledBitmap(wallBmp, gridSize, gridSize, true);
        emptyBmp = Bitmap.createScaledBitmap(emptyBmp, gridSize, gridSize, true);
        //blackQuad = Bitmap.createScaledBitmap(blackQuad, ScreenWidth, ScreenHeight, true);

        smokeBmp = BitmapFactory.decodeResource(_View.getResources(),R.drawable.justsmoke);
        smokeBmp = Bitmap.createScaledBitmap(smokeBmp, ScreenWidth * 6 ,ScreenHeight,true);

        spriteSheet = new Sprite(smokeBmp,1,2,3);
        spriteSheet.SetAlpha(0);

        EntityManager.Instance.Init(_View);
        //System.out.println("Grid size: " + GameDataManager.Instance.GetGridSize());
        DataManager.Instance.SetCurrentLevel(0);

        data = DataManager.Instance.dataMap.get(DataManager.Instance.GetCurrentLevel()).tilesArray;
//        for(int i = 0;i < data.length; ++i)
//        {
//            if(data[i].equals(Data.TILES.SPRITE)) {
//                //System.out.println("Created sample entity. X:" +(i % noOfGrid) * gridSize + "Y: " + (i / noOfGrid) * gridSize);
//                Entity_Character.SampleEntityCreate(gridOffset + (float)(i % noOfGrid) * gridSize, gridOffset +(float)(i / noOfGrid) * gridSize);
//            }
//        }
        dataMap = DataManager.Instance.dataMap.get(DataManager.Instance.GetCurrentLevel()).walkableMap;

        RenderTextEntity.Create();

        //Main character
        //Entity_Character.Create(ScreenWidth*0.1f, ScreenHeight* 0.47f);

        //Hardcode first

        Vector3 water = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.84f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.74f  / GameDataManager.Instance.GetGridSize()));


        LevelManager.Instance.AddInteractable("TAPLEAK",  water);

        //Entity_Interactables.Create(1600,800,Entity_Interactables.TYPE.WATER);
        //Entity_Obstacle.Create(1500, 700);
/*
        Entity_NPC.Create(ScreenWidth*0.9f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_Button.Create(ScreenWidth*0.9f, ScreenHeight *0.85f);

        Entity_Obstacle.Create(1500, 700);

        Entity_PauseButton.Create(ScreenWidth*0.9f, ScreenHeight *0.01f);

        RenderBackground.Create();
*/

        Entity_Interactables.CreateTelevisionIntoList(ScreenWidth * 0.13f, ScreenHeight * 0.62f,false);

        Entity_NPC.Create(ScreenWidth*0.9f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.8f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_Button.Create(ScreenWidth*0.9f, ScreenHeight *0.825f);

        //Entity_Obstacle.Create(1500, 700);

        Entity_PauseButton.Create(ScreenWidth*0.9f, ScreenHeight *0.01f);

        RenderBackground.Create();

        Entity_InteractButton.Create(ScreenWidth*0.9f, ScreenHeight *0.65f);;

        Entity_SprayButton.Create(ScreenWidth*0.9f, ScreenHeight *0.825f);;

        Entity_FireExtinguisher.Create(ScreenWidth * 0.2f, ScreenHeight * 0.5f);

//        Entity_Interactables.Create(ScreenWidth * 0.80f, ScreenHeight * 0.45f,Entity_Interactables.TYPE.TELEVISION);



        //Hardcode first
        Vector3 tele = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.13f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.62f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("TELEVISION",  tele);

        Vector3 fire = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.54f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.21f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("FIRE",  fire);

        //Main character
        Entity_Character.Create(ScreenWidth*0.1f, ScreenHeight* 0.47f);
        initialized = true;

        AudioManager.Instance.PlayAudio(R.raw.bensound_littleidea, 0.75f,1);
        view = _View;
    }

    @Override
    public void OnExit()
    {

    }

    public void Init(SurfaceView _View) {
        Grid.Instance.CreateGrid(0);
        LevelManager.Instance.ClearEntireMap();
        CharacterData.Instance.money = 200;

        AudioManager.Instance.Init(_View);

        //Init the variables
        gridSize = GameDataManager.Instance.GetGridSize();
        noOfGrid = GameDataManager.Instance.GetNoOfGrid();
        gridOffset = GameDataManager.Instance.GetGridOffset();
        ScreenWidth = GameDataManager.Instance.GetScreenWidth();
        ScreenHeight = GameDataManager.Instance.GetScreenHeight();
        //Load Resources for e.g. Images

        backgroundbmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.tilemap);
        emptyBmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.icon);
        wallBmp = BitmapFactory.decodeResource(_View.getResources(), R.drawable.justblack);
        exmark= BitmapFactory.decodeResource(_View.getResources(), R.drawable.exmark);
        blackQuad = BitmapFactory.decodeResource(_View.getResources(),R.drawable.black);
        //button_popup = BitmapFactory.decodeResource(_View.getResources(), R.drawable.buttonxml);
//*/
        //Scaled version of bmp which is background of game scene
        backgroundbmp = Bitmap.createScaledBitmap(backgroundbmp, ScreenWidth, ScreenHeight, true);
        exmark = Bitmap.createScaledBitmap(exmark, gridSize * 2, gridSize * 2, true);
        //wallBmp = Bitmap.createScaledBitmap(wallBmp, gridSize, gridSize, true);
        emptyBmp = Bitmap.createScaledBitmap(emptyBmp, gridSize, gridSize, true);
        //blackQuad = Bitmap.createScaledBitmap(blackQuad, ScreenWidth, ScreenHeight, true);

        DataManager.Instance.SetCurrentLevel(0);
        EntityManager.Instance.Init(_View);
        //System.out.println("Grid size: " + GameDataManager.Instance.GetGridSize());

        data = DataManager.Instance.dataMap.get(DataManager.Instance.GetCurrentLevel()).tilesArray;
//        for(int i = 0;i < data.length; ++i)
//        {
//            if(data[i].equals(Data.TILES.SPRITE)) {
//                //System.out.println("Created sample entity. X:" +(i % noOfGrid) * gridSize + "Y: " + (i / noOfGrid) * gridSize);
//                Entity_Character.SampleEntityCreate(gridOffset + (float)(i % noOfGrid) * gridSize, gridOffset +(float)(i / noOfGrid) * gridSize);
//            }
//        }
        dataMap = DataManager.Instance.dataMap.get(DataManager.Instance.GetCurrentLevel()).walkableMap;

        RenderTextEntity.Create();


        //Hardcode first
        Vector3 water = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.84f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.74f  / GameDataManager.Instance.GetGridSize()));

  //          LevelManager.Instance.AddInteractable("TAPLEAK",  water);

//        Entity_Interactables.Create(ScreenWidth * 0.15f, ScreenHeight * 0.15f,Entity_Interactables.TYPE.FIRE);
        //Entity_Obstacle.Create(1500, 700);

 //Commented out part begins****************

        Entity_Interactables.CreateTelevisionIntoList(ScreenWidth * 0.13f, ScreenHeight * 0.62f,false);

        Entity_NPC.Create(ScreenWidth*0.9f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_NPC.Create(ScreenWidth*0.8f, ScreenHeight* 0.27f ,Entity_NPC.NPC_TYPE.MAN);
        Entity_Button.Create(ScreenWidth*0.9f, ScreenHeight *0.825f);

        //Entity_Obstacle.Create(1500, 700);

        Entity_PauseButton.Create(ScreenWidth*0.9f, ScreenHeight *0.01f);

        RenderBackground.Create();

        Entity_InteractButton.Create(ScreenWidth*0.9f, ScreenHeight *0.65f);;

        Entity_SprayButton.Create(ScreenWidth*0.9f, ScreenHeight *0.825f);;

        Entity_FireExtinguisher.Create(ScreenWidth * 0.2f, ScreenHeight * 0.5f);

//        Entity_Interactables.Create(ScreenWidth * 0.80f, ScreenHeight * 0.45f,Entity_Interactables.TYPE.TELEVISION);



        //Hardcode first
        Vector3 tele = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.13f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.62f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("TELEVISION",  tele);

        Vector3 fire = new Vector3((int) Math.floor(GameDataManager.Instance.GetScreenWidth() * 0.54f  / GameDataManager.Instance.GetGridSize()),
                (int) Math.floor(GameDataManager.Instance.GetScreenHeight() * 0.21f  / GameDataManager.Instance.GetGridSize()));

        LevelManager.Instance.AddInteractable("FIRE",  fire);

        //Main character
        Entity_Character.Create(ScreenWidth*0.1f, ScreenHeight* 0.47f);
        initialized = true;

        AudioManager.Instance.PlayAudio(R.raw.bensound_littleidea, 0.75f,1);
        view =_View;

    }
static float moneyMinusTimer = 1;
    static float testTimer = 1;

    public void Update(float _deltaTime) {

        System.out.println(DataSaver.Instance.GetIntFromSave("highscore"));
        spriteSheet.Update(_deltaTime);

        {
            if (xPos < -ScreenWidth) {
                xPos = 0;
            }

            float hour = GameDataManager.Instance.getHourOfTheDay();
            hour += 0.5f * _deltaTime;
            if (hour >= 24.f) {
                if(DataSaver.Instance.GetIntFromSave("highscorelevel1") < CharacterData.Instance.money) {
                   DataSaver.Instance.SaveEditBegin();

                    DataSaver.Instance.SetIntInSave("highscorelevel1", CharacterData.Instance.money);
                    DataSaver.Instance.SaveEditEnd();
                }
                view.getContext().startActivity(new Intent(view.getContext(), ScreenWin.class));
                hour = 0;
            }
            GameDataManager.Instance.setHourOfTheDay(hour);

            if(CharacterData.Instance.money <=0)
            {
                view.getContext().startActivity(new Intent(view.getContext(), ScreenLose.class));
            }

            elapsedTime += _deltaTime;
            if (elapsedTime > 3.0f) {
                int randPosX = seed.nextInt(ScreenWidth / gridSize);
                int randPosY = seed.nextInt(ScreenHeight / gridSize);

                //Entity_Obstacle.Create(randPosX * gridSize + gridOffset, randPosY * gridSize + gridOffset);
/*
            DataManager.Instance.dataMap.get(0).intArray[(randPosY * noOfGrid + randPosX)] = 2;
            DataManager.Instance.dataMap.get(0).UpdateTileArray();
            DataManager.Instance.dataMap.get(0).UpdateWalkableAreas();
*/
                elapsedTime = 0;
            }
            if (GameDataManager.Instance.interactableList != null) {
                moneyMinusTimer -= _deltaTime;
                if (moneyMinusTimer <= 0) {
                    int money = CharacterData.Instance.getMoney();
                    money -= (GameDataManager.Instance.interactableList.size()) * 2;
                    CharacterData.Instance.setMoney(money);
                    moneyMinusTimer = 1;
                }

            }
            EntityManager.Instance.Update(_deltaTime);

            if(!CharacterData.Instance.isOnFire)
            {
                float minAlpha = 0.f;
                if(spriteSheet.GetAlpha() > minAlpha)
                {
                    spriteSheet.SetAlpha(spriteSheet.GetAlpha() -  40 * _deltaTime);
                }
            }
            else
            {
                float maxAlpha = 120.f;
                if(spriteSheet.GetAlpha() < maxAlpha)
                {
                    spriteSheet.SetAlpha(spriteSheet.GetAlpha() + 10 * _deltaTime);
                }
            }
        }
    }

    public void Render(Canvas _canvas) {
        //draw something
        _canvas.drawBitmap(backgroundbmp, xPos, yPos, null); //left = x, top = y, counting from top left corner(0,0)
//
//        for (int i = 0; i < data.length; ++i) {
//            if (data[i] == null) {
//                data[i] = Data.TILES.EMPTY;
//            }
//            switch (data[i]) {
//                case WALLS: {
//                    //  _canvas.drawBitmap(SwallBmp, (i % noOfGrid)* gridSize, (i / noOfGrid) * gridSize, null); //left = x, top = y, counting from top left corner(0,0)
//                    break;
//                }
//                case EMPTY: {
//                    //_canvas.drawBitmap(SemptyBmp,  (i % noOfGrid)* gridSize, (i / noOfGrid) * gridSize, null); //left = x, top = y, counting from top left corner(0,0)
//                    break;
//                }
//                case DEFAULT: {
//                    break;
//                }
//            }
//        }

        if(GameDataManager.Instance.interactableList != null) {
            //Want to render the exclamation above this
            for (Entity_Interactables entity : GameDataManager.Instance.interactableList) {
                if(entity.type != Entity_Interactables.TYPE.RUBBISH) {
                    _canvas.drawBitmap(exmark, entity.pos.x, entity.pos.y - entity.GetRadius(), null); //left = x, top = y, counting from top left corner(0,0)
                }
            }
        }

        //Render entity
        EntityManager.Instance.Render(_canvas);

        spriteSheet.Render(_canvas,ScreenWidth / 2, ScreenHeight /2);
/*
        Paint paint = new Paint();
        paint.setARGB(155,0,0,0);
        _canvas.drawBitmap(blackQuad, xPos, yPos, paint );
*/

    }

    public void Debug(Canvas _canvas)
    {
            Paint paint = new Paint();
            paint.setAlpha(160);
            for(Map.Entry<Integer,Boolean> entry : dataMap.entrySet()) {
                Integer index = entry.getKey();
                Boolean walkable = entry.getValue();
                if (!walkable) {
                    _canvas.drawBitmap(wallBmp, (index % noOfGrid) * gridSize, gridOffset + (index / noOfGrid) * gridSize, paint);
                } else {
                    _canvas.drawBitmap(emptyBmp, (index % noOfGrid) * gridSize, gridOffset +(index / noOfGrid) * gridSize, paint);
                }
            }

        if (Grid.Instance.path != null && !Grid.Instance.path.isEmpty()) {
            Queue<Node> tempQueue = new LinkedList<Node>();
            for (Node var : Grid.Instance.path) {
                _canvas.drawBitmap(wallBmp, (var.gridX) * gridSize, gridOffset + (var.gridY) * gridSize, null);
            }
        }
    }

    @Override
    public void SetIsPaused(boolean _pauseStatus)
    {
        IsPaused = _pauseStatus;
        return;
    }

    @Override
    public boolean GetIsPaused()
    {
        return IsPaused;
    }




}

