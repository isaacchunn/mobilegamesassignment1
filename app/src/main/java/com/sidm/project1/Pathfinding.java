package com.sidm.project1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class Pathfinding {

    public final static Pathfinding Instance = new Pathfinding();
    private Pathfinding() {
    }

    void FindPath(Node startNode, Node endNode)
    {
        Node start = startNode;
        Node end = endNode;
        ArrayList<Node> openSet = new ArrayList<Node>();
        ArrayList<Node> closedSet = new ArrayList<Node>();
        openSet.add(start);

        while(openSet.size() >0)
        {
            Node node = openSet.get(0);
            for(int i =1;i < openSet.size(); i++)
            {
                if(openSet.get(i).fCost() < node.fCost() || openSet.get(i).fCost() == node.fCost())
                {
                    if(openSet.get(i).hCost < node.hCost)
                        node = openSet.get(i);
                }
            }

            openSet.remove(node);
            closedSet.add(node);

            if(node.equals(end))
            {
                //System.out.println("End Node Parent" + endNode.parent.gridX + "," + endNode.parent.gridY);
                RetracePath(start,end,start.parent,end.parent);
                return;
            }

            for (Node neighbour : Grid.Instance.GetNeighbours(node)) {
                if(!neighbour.walkable || closedSet.contains(neighbour)) {
                    continue;
                }

                int newCostToNeighbour = node.gCost + GetDistance(node, neighbour);
                if(newCostToNeighbour < neighbour.gCost || !openSet.contains(neighbour))
                {
                    neighbour.gCost = newCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, end);
                    neighbour.parent = node;
                    if(neighbour.equals(end))
                    {
                          end.parent = node;

                    }
                    if(!openSet.contains(neighbour))
                    {
                        openSet.add(neighbour);
                    }
                }
            }
        }
    }

    void RetracePath(Node startNode, Node endNode,Node startParent, Node endParent)
    {
        ArrayList<Node> path = new ArrayList<Node>();
        Node start = startNode;
        startNode.parent = startParent;

        Node currentNode = endNode;
        currentNode.parent = endParent;

        while(!currentNode.equals(start))
        {
           path.add(currentNode);
            currentNode = currentNode.parent;
        }

        Collections.reverse(path);

//        for(Node var : path)
//        {
//            System.out.println("Path:" + var.gridX + "," +var.gridY);
//        }

        Grid.Instance.path = path;
    }

    int GetDistance( Node node, Node node2)
    {
        int dstX =Math.abs(node.gridX - node2.gridX);
        int dstY = Math.abs(node.gridY - node2.gridY);

        if(dstX > dstY)
            return 14*dstY + 10 * (dstX - dstY);
        return 14 *dstX + 10 * (dstY - dstX);
    }

}
