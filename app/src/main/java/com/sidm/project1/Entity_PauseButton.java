package com.sidm.project1;

import android.app.Activity;
import android.content.Entity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

//use collision code for button ecks dee
public class Entity_PauseButton implements EntityBase, Collidable {

    public final static Entity_PauseButton Instance = new Entity_PauseButton(0,0);
    public Vector3 pos;
    boolean isDone = false;
    private boolean isInit = false;
    private Bitmap bmp = null;
    private Bitmap scaledbmp = null;
    public boolean active = false;
    GamePage gamePageReference = new GamePage();
    enum BUTTON_TYPE
    {
        CIRCLE,
        RECT,
    }
    Entity_PauseButton(float xPosition, float yPosition)
    {
        pos = new Vector3(xPosition,yPosition);

    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }
    @Override
    public boolean IsInit() {
        return isInit;
    }
    @Override
    public void Init(SurfaceView _view) {
        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.settings); //other images
        scaledbmp = Bitmap.createScaledBitmap(bmp, GameDataManager.Instance.GetGridSize() * 3, GameDataManager.Instance.GetGridSize() * 3, true);
    }

    @Override
    public void Update(float _dt) {
        // Update based on dt
    }

    @Override
    public void Render(Canvas _canvas) {
        _canvas.drawBitmap(scaledbmp, pos.x, pos.y, null); //left = x, top = y, counting from top left corner(0,0)
    }

    public static Entity_PauseButton Create(float xPosition, float yPosition)
    {
        //Entity_PauseButton result = new Entity_PauseButton(xPosition,yPosition);
        //EntityManager.Instance.AddEntity(result);
        Entity_PauseButton.Instance.SetPosX(xPosition);
        Entity_PauseButton.Instance.SetPosY(yPosition);
        Entity_PauseButton.Instance.active = true;
        Entity_PauseButton result = Entity_PauseButton.Instance;
        if(EntityManager.Instance.entityList.indexOf(result) == -1 ) //== -1, means not in list
        {
            EntityManager.Instance.AddEntity(result);
        }

        return result;
    }

    @Override
    public String GetType() {
        return "Entity_PauseButton";
    }

    @Override
    public float GetPosX() {
        return pos.x;
    }

    @Override
    public float GetPosY() {
        return pos.y;
    }

    public void SetPosX(float posX){pos.x = posX;}

    public void SetPosY(float posY){pos.y = posY;}

    @Override
    public float GetRadius()
    {
        return scaledbmp.getHeight()*0.5f;

    }
    @Override
    public void OnHit(Collidable _other)
    {
        //if(_other.GetType() == "Entity_Character")
        //{
        //    SetIsDone(true);
        //}
        //startActivity(new Intent(GamePage.class., PopupMessageWindow.class));
    }
    @Override
    public void TouchHit()
    {
        SampleGame.Instance.SetIsPaused(!SampleGame.Instance.GetIsPaused());
        //gamePageReference.openMenu();
    }

    @Override
    public boolean GetAttending()
    {
        return false;
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.UI_LAYER;
    }
    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
}
