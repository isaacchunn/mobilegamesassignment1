package com.sidm.project1;

import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataReader {
    public final static DataReader Instance = new DataReader();
    private DataReader() {
    }

    //Specific csv that reads int only
    public Data readCSV(Context context, int excelID) {
        //Initialise the buffered reader
       BufferedReader reader = null;
       //Initialise an input stream by trying to get the ID from the raw file, if not exception.
       InputStream stream = context.getResources().openRawResource(excelID);

       //Read the file...
       InputStreamReader ireader = new InputStreamReader(stream);
        String csvLine= ""; //line
        String csvSplitBy = ","; //the regex
        String combinedString = ""; //the string to be passed into the data class to print the entire file
        ArrayList list = new ArrayList();
        ArrayList<String>stringList = new ArrayList<String>();
        int index = 0 ;
        int vertIndex = 0;
        try {
             reader = new BufferedReader(ireader);
            while((csvLine = reader.readLine()) != null)
            {
                String[] inputLine = csvLine.split(csvSplitBy);

                combinedString += csvLine + "\n";

                for(int i = 0 ; i < inputLine.length ; ++i)
                {
                    list.add(Integer.parseInt(inputLine[i]));
                }
            }
            String[] grid = combinedString.split("\n");

            //To get the height of the csv
            for(int i =0; i < grid.length; ++i)
            {
                stringList.add(grid[i]);
                vertIndex++;
            }
            //To get the number of rows that is in each row
            String[] stringIndex = stringList.get(0).split(csvSplitBy);
            for(int i =0; i < stringIndex.length; ++i)
            {
                ++index;
            }

            System.out.println("Index:" + index+ " VertIndex: "+ vertIndex + "List String" + stringList.get(0));
            return new Data(list,combinedString,index, vertIndex);

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        ArrayList empty = new ArrayList();
        String emptyString = "";
        return new Data(empty,emptyString,0,0);


        //System.out.println("Successfully read, now returning.");
//    return temp;
    }


}

