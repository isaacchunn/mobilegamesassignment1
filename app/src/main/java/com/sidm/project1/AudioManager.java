package com.sidm.project1;

import android.content.res.Resources;
import android.media.MediaPlayer;
import android.view.SurfaceView;

import java.util.HashMap;

public class AudioManager {
    // This is our singleton
    public final static AudioManager Instance = new AudioManager();

    private Resources res = null;
    private SurfaceView view = null;
    private HashMap<Integer, MediaPlayer> audioMap = new HashMap<Integer, MediaPlayer>();


    // Protect da singleton!!!
    private AudioManager() {
    }

    public void Init(SurfaceView _view) {
        view = _view;
        Exit();
    }

    // Method 2
    // Adjust your sound from 0.0
    /*public void PlayAudio(int _id, int volume)
    {
        // Check if the audio is loaded or not
        if (audioMap.containsKey(_id))
        {

            audioMap.get(_id).reset();
            audioMap.get(_id).start();

        }

        // Load the audio
        MediaPlayer newAudio = MediaPlayer.create(view.getContext(), _id);
        audioMap.put(_id, newAudio);

        newAudio.setVolume(volume,volume); // Left and right volume
        newAudio.start(); // Just play the audio immediately
    }

    public boolean IsPlaying(int _id)
    {
        if (!audioMap.containsKey(_id))
            return false;

        return audioMap.get(_id).isPlaying();
    }*/

    // Method 1
    public void PlayAudio(int _id, float _volume, float speed) {
        // Check if the audio is loaded or not
        if (audioMap.containsKey(_id)) {
            // We got it!
            MediaPlayer curr = audioMap.get(_id);
            //curr.seekTo(0);
            curr.setVolume(_volume, _volume);
            curr.start();
        } else {
            // Load the audio
            MediaPlayer curr = MediaPlayer.create(view.getContext(), _id);
            curr.setVolume(_volume, _volume);
            audioMap.put(_id, curr);
            curr.start(); // Just play the audio immediately
        }

    }

    public void StopAudio(int _id) {
        MediaPlayer Audio = audioMap.get(_id);
        Audio.pause();
    }

    public void Exit() {
        for (HashMap.Entry<Integer, MediaPlayer> entry : audioMap.entrySet()) {
            entry.getValue().stop();
            entry.getValue().reset();
            entry.getValue().release();
        }
        audioMap.clear();
    }

    public void SetVolume(int _id, float _volume) {
        if (audioMap.containsKey(_id)) {
            MediaPlayer curr = audioMap.get(_id);
            //curr.seekTo(0);
            curr.setVolume(_volume, _volume);
        }
        else
        {
            // Load the audio
            MediaPlayer curr = MediaPlayer.create(view.getContext(), _id);
            curr.setVolume(_volume, _volume);
            audioMap.put(_id, curr);
            curr.start(); // Just play the audio immediately
        }
    }
}


