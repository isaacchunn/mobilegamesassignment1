package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceView;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.logging.Level;

import static com.sidm.project1.Entity_NPC.STATE.FIRE;
import static com.sidm.project1.Entity_NPC.STATE.NONE;
import static com.sidm.project1.Entity_NPC.STATE.TAPLEAK;
import static com.sidm.project1.Entity_NPC.STATE.TELEVISION;
import static com.sidm.project1.Entity_NPC.STATE.WANDER;

public class Entity_NPC implements EntityBase, Collidable {

    public Vector3 pos;
    public Vector3 target;

    int currentGridX, currentGridY;

    private boolean isInit = false;
    boolean isDone = false;
    private Sprite spriteUp = null;
    private Sprite spriteLeft = null;
    private Sprite spriteDown = null;
    private Sprite spriteRight = null;

    final float defaultMoveSpeed = 250f;
    final float boostedMoveSpeed = 400f;

    float delayPath = 0;

    boolean attempting = false;
    boolean attempted = false;
    float timeToAttempt = 2;
    static float rubbishDrop = 0;
    boolean turnTele = false;

    public float moveSpeed = defaultMoveSpeed; // big value for big bois
    Queue<Vector3> vector3Queue = new LinkedList<Vector3>();

    enum NPC_TYPE {
        BABY,
        MAN,
    }

    enum MOVEMENT {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    enum STATE
    {
        WANDER,
        TAPLEAK,
        FIRE,
        LIGHTS,
        TELEVISION,
        NONE,
    }
    STATE state;

    Queue<Entity_NPC.MOVEMENT> movementQueue = new LinkedList<Entity_NPC.MOVEMENT>();
    MOVEMENT currentMovement;

    NPC_TYPE type;

    public Entity_NPC(float xPosition, float yPosition) {
        pos = new Vector3(xPosition, yPosition);
        type = NPC_TYPE.BABY;
        target = pos;
        state = WANDER;

    }

    public Entity_NPC(float xPosition, float yPosition, NPC_TYPE type) {
        pos = new Vector3(xPosition, yPosition);
        this.type = type;
        target = pos;
        state = WANDER;
    }

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public boolean IsInit() {
        return isInit;
    }

    @Override
    public void Init(SurfaceView _view) {

        switch(type)
        {
            case BABY:
                spriteUp = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.charup), 1, 3, 14);
                spriteDown = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.chardown), 1, 3, 14);
                spriteRight = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.charright), 1, 3, 14);
                spriteLeft = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.charleft), 1, 3, 14);
                break;
            case MAN:
                spriteUp = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.npc1up), 1, 3, 14);
                spriteDown = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.npc1down), 1, 3, 14);
                spriteRight = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.npcright), 1, 3, 14);
                spriteLeft = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.npc1left), 1, 3, 14);
                break;
        }
    }

    @Override
    public void Update(float _dt) {
        // Update based on dt
        //Do update.
        //calculate the currentGrid;
        currentGridX = (int) Math.floor(pos.x / GameDataManager.Instance.GetGridSize());
        currentGridY = (int) Math.floor(pos.y / GameDataManager.Instance.GetGridSize());

        ChooseActions(_dt);

        //Movement section
        Vector3 dir = Vector3.Sub(target, pos);
        if (dir.Length() < moveSpeed * _dt) {
            //System.out.println("Reached");
            pos = target;
            if (!vector3Queue.isEmpty()) {
                target = vector3Queue.peek();
                vector3Queue.remove();
                if (!movementQueue.isEmpty()) {
                    if (movementQueue.size() == 1) // last element
                    {
                        if(state == WANDER) {
                            state = NONE;
                        }
                        currentMovement = movementQueue.peek();
                    }
                    movementQueue.remove();
                }
            }

        } else {
            try {
                dir.Normalize();
                dir.Mult(moveSpeed * _dt);
                pos.Add(dir);
//             System.out.println(pos.toString());
                rubbishDrop += _dt;
                if(rubbishDrop >= 5.0f)
                {
                    if(GameDataManager.Instance.GetCurrentRubbish() < GameDataManager.Instance.GetMaxRubbish()) {
                        Entity_Interactables.CreateIntoList(pos.x, pos.y, Entity_Interactables.TYPE.RUBBISH);
                        GameDataManager.Instance.SetCurrentRubbish(GameDataManager.Instance.GetCurrentRubbish()+1);
                        rubbishDrop = 0;
                    }
                }
            } catch (Exception e) {

            }
        }

        //Update the sprites
        if (!movementQueue.isEmpty()) {
            switch (movementQueue.peek()) {
                case DOWN:
                    spriteDown.Update(_dt);
                    break;
                case UP:
                    spriteUp.Update(_dt);
                    break;
                case LEFT:
                    spriteLeft.Update(_dt);
                    ;
                    break;
                case RIGHT:
                    spriteRight.Update(_dt);
                    break;
            }
        }
    }

    @Override
    public void Render(Canvas _canvas) {
        if (!movementQueue.isEmpty()) {
            switch (movementQueue.peek()) {
                case DOWN:
                    spriteDown.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                    break;
                case UP:
                    spriteUp.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                    break;
                case LEFT:
                    spriteLeft.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                    break;
                case RIGHT:
                    spriteRight.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                    break;
            }
        } else {
            if (currentMovement != null) {
                switch (currentMovement) {
                    case DOWN:
                        spriteDown.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                        break;
                    case UP:
                        spriteUp.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                        break;
                    case LEFT:
                        spriteLeft.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                        break;
                    case RIGHT:
                        spriteRight.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
                        break;
                }
            } else {
                spriteDown.Render(_canvas, GameDataManager.Instance.GetGridOffset() + (int) pos.x, GameDataManager.Instance.GetGridOffset() + (int) pos.y);
            }
        }
    }

    public static Entity_NPC Create(float xPosition, float yPosition) {
        Entity_NPC result = new Entity_NPC(xPosition, yPosition);
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    public static Entity_NPC Create(float xPosition, float yPosition, NPC_TYPE type) {
        Entity_NPC result = new Entity_NPC(xPosition, yPosition,type);
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public String GetType() {
        return "Entity_NPC";
    }

    @Override
    public float GetPosX() {
        return pos.x;
    }

    @Override
    public float GetPosY() {
        return pos.y;
    }

    @Override
    public float GetRadius() {
        if (currentMovement != null) {
            switch (currentMovement) {
                case RIGHT:
                    return spriteRight.GetHeight() * 0.5f;
                case LEFT:
                    return spriteLeft.GetHeight() * 0.5f;
                case UP:
                    return spriteUp.GetHeight() * 0.5f;
                case DOWN:
                    return spriteDown.GetHeight() * 0.5f;
            }
        }

        return spriteUp.GetHeight() * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other) {

        if(_other.GetType() == "Entity_Character")
        {
//            Texts text = new Texts("Char", 2, new Vector3(pos.x,pos.y + GetRadius()),true);
//            RenderTextEntity.textList.add(text);
           // SetIsDone(true);
            //text = null;
        }
//        else if(_other.GetType() == "Entity_Interactable")
//        {
//
//            Texts text = new Texts("Attempting", 2, new Vector3(pos.x,pos.y + GetRadius()),true);
//            RenderTextEntity.textList.add(text);
//        }

    }

    @Override
    public void TouchHit() {

    }

    @Override
    public boolean GetAttending()
    {
        return turnTele;
    }

    void UpdateSprite() {
        //Clear the queue.
        while (!movementQueue.isEmpty()) {
            movementQueue.remove(); // reset vector queue // cant use claer cause abstract class
        }

        for (int i = 1; i < Grid.Instance.path.size(); ++i) {
            Node previous, current;
            previous = Grid.Instance.path.get(i - 1);
            current = Grid.Instance.path.get(i);

            //Check right
            if (current.gridX > previous.gridX && current.gridY == previous.gridY) {
                movementQueue.add(MOVEMENT.RIGHT);
            }
            //Diagonal top right and bottom right
            else if (current.gridX > previous.gridX && current.gridY > previous.gridY) {
                movementQueue.add(MOVEMENT.RIGHT);
            }
            //Check left
            else if (current.gridX < previous.gridX && current.gridY == previous.gridY) {
                movementQueue.add(MOVEMENT.LEFT);
            }
            //Diagonal top left and bottom left
            else if (current.gridX < previous.gridX && current.gridY < previous.gridY) {
                movementQueue.add(MOVEMENT.LEFT);
            }
            //Check up
            else if (current.gridY > previous.gridY && current.gridX == previous.gridX) {
                movementQueue.add(MOVEMENT.DOWN);
            }
            //Check bottom
            else if (current.gridY < previous.gridY && current.gridX == previous.gridX) {
                movementQueue.add(MOVEMENT.UP);
            }

            previous = null; //save memory?
            current = null; //save memory?
        }
    }

    void Pathfinding(int targetGridX, int targetGridY) {
        if (Grid.Instance.grid[targetGridX][targetGridY].walkable) {
            while (!vector3Queue.isEmpty()) {
                vector3Queue.remove(); // reset vector queue // cant use claer cause abstract class
            }
            Pathfinding.Instance.FindPath(new Node(false, null, currentGridX, currentGridY),
                    new Node(false, null, targetGridX, targetGridY));
            if(Grid.Instance.path != null ) {
                for (Node var : Grid.Instance.path) {
                    vector3Queue.add(new Vector3(var.gridX * GameDataManager.Instance.GetGridSize(),
                            var.gridY * GameDataManager.Instance.GetGridSize(),
                            0));
                }
                UpdateSprite();
            }

        } else {
            if (vector3Queue.isEmpty()) {
                for (Node neighbours : Grid.Instance.GetNeighbours(Grid.Instance.grid[targetGridX][targetGridY])
                        ) {
                    // System.out.println("Point clicked was: X: " + gridX + "Y:" + gridY + "Neighbour: X:" + neighbours.gridX + "Y:" + neighbours.gridY);
                    if (neighbours.walkable) {
                        Pathfinding.Instance.FindPath(new Node(false, null, currentGridX, currentGridY), new Node(false, null, neighbours.gridX, neighbours.gridY));
                        //System.out.println("Path found in neighbour::");
                        if(Grid.Instance.path != null ) {
                            for (Node var : Grid.Instance.path) {
                                //System.out.println(var.gridX + "," + var.gridY);
                                vector3Queue.add(new Vector3(var.gridX * GameDataManager.Instance.GetGridSize(), var.gridY * GameDataManager.Instance.GetGridSize(), 0));
                            }
                            UpdateSprite();
                        }
                        break;
                    }
                }
            }
        }
    }

    void ChooseActions(float _dt)
    {
        switch(state)
        {
            case NONE:
                Random randome = new Random();
                int stateOscillation = randome.nextInt(100) + 1;
                //System.out.println("stateOscillation:" + stateOscillation);

                if(stateOscillation <= 15) {
                    if(LevelManager.Instance.GetInteractable("TAPLEAK") != null) {
                        state = TAPLEAK;
                    }
                    else
                    {
                        System.out.println("Chose tap but it was full");
                        state = WANDER;
                    }
                }
                else if (stateOscillation <= 30) {
                    System.out.println("TV");
                    if(LevelManager.Instance.GetInteractable("TELEVISION")!=null) {
                        state = TELEVISION;
                    }
                    else
                    {
                        System.out.println("Chose television but it was full");
                        state = WANDER;
                    }
                }
                else if (stateOscillation <= 45) {
                    if(LevelManager.Instance.GetInteractable("FIRE") !=null) {
                        state = FIRE;
                    }
                    else
                    {
                        System.out.println("Chose fire but it was full");
                        state = WANDER;
                    }
                }
                else if (stateOscillation <= 100) {
                    state = WANDER;
                }
                break;
            case WANDER:
                // System.out.println("WANDER");
                if(vector3Queue.isEmpty()) {
                    if(Grid.Instance.walkableList != null) {
                        delayPath += _dt;
                        if (delayPath >= 2.5f) {
                            Random rand = new Random();

                            int random = rand.nextInt(Grid.Instance.walkableList.size() - 1);
                            Node tempNode = Grid.Instance.walkableList.get(random);

                            Pathfinding(tempNode.gridX, tempNode.gridY);
                            delayPath = 0;
                        }
                    }
                }
                break;
            case TAPLEAK:
                //hardcore first to test

                int currentGridX = (int) Math.floor(pos.x / GameDataManager.Instance.GetGridSize());
                int currentGridY = (int) Math.floor(pos.y / GameDataManager.Instance.GetGridSize());

                Vector3 gridVector = LevelManager.Instance.GetInteractblePos("TAPLEAK");

                if(currentGridX != gridVector.x && currentGridY != gridVector.y ) {
                    Pathfinding((int)gridVector.x, (int)gridVector.y);
                }
                else
                {
                    attempting = true;
                    //System.out.println("Attempting" + timeToAttempt + "Attempting:" + attempting);
                }
                if (attempting) {

                    timeToAttempt -= _dt;
                    if (timeToAttempt <= 0) {
                        attempting = false;
                        attempted = true;
                        timeToAttempt = 2;
                    }
                }
                if (attempted) {
                    //sorry hardcode first
                    //EntityManager.Instance.addList.add(new Entity_Interactables(1600,800,Entity_Interactables.TYPE.WATER));
                    LevelManager.Instance.booleanMap.put("TAPLEAK", true);
                    Entity_Interactables.CreateIntoList(gridVector.x * GameDataManager.Instance.GetGridSize(),gridVector.y * GameDataManager.Instance.GetGridSize(),Entity_Interactables.TYPE.WATER);
                    attempted = false;
                    state = NONE;
                }
                attempting = false;

                break;

            case TELEVISION:
                int currentX = (int) Math.floor(pos.x / GameDataManager.Instance.GetGridSize());
                int currentY = (int) Math.floor(pos.y / GameDataManager.Instance.GetGridSize());

                Vector3 grid = LevelManager.Instance.GetInteractblePos("TELEVISION");

                if(currentX != grid.x && currentY != grid.y ) {
                    Pathfinding((int)grid.x, (int)grid.y);
                }
                else
                {
                    attempting = true;
                    LevelManager.Instance.booleanMap.put("TELEVISION", true);

                    //System.out.println("Attempting" + timeToAttempt + "Attempting:" + attempting);
                }
                if (attempting) {

                    timeToAttempt -= _dt;
                    if (timeToAttempt <= 0) {
                        attempting = false;
                        attempted = true;
                        timeToAttempt = 3;
                    }
                }
                if (attempted) {

                    attempted = false;
                    state = NONE;
                }
                attempting = false;
                break;
            case FIRE:
                int currX = (int) Math.floor(pos.x / GameDataManager.Instance.GetGridSize());
                int currY = (int) Math.floor(pos.y / GameDataManager.Instance.GetGridSize());

                Vector3 gridVec = LevelManager.Instance.GetInteractblePos("FIRE");

                if(currX != gridVec.x && currY != gridVec.y ) {
                    Pathfinding((int)gridVec.x, (int)gridVec.y);
                }
                else
                {
                    attempting = true;
                    //System.out.println("Attempting" + timeToAttempt + "Attempting:" + attempting);
                }
                if (attempting) {

                    timeToAttempt -= _dt;
                    if (timeToAttempt <= 0) {
                        attempting = false;
                        attempted = true;
                        timeToAttempt = 2;
                    }
                }
                if (attempted) {
                    //sorry hardcode first
                    //EntityManager.Instance.addList.add(new Entity_Interactables(1600,800,Entity_Interactables.TYPE.WATER));
                    LevelManager.Instance.booleanMap.put("FIRE", true);
                    Entity_Interactables.CreateIntoList(gridVec.x * GameDataManager.Instance.GetGridSize(),gridVec.y * GameDataManager.Instance.GetGridSize(),Entity_Interactables.TYPE.FIRE);
                    attempted = false;
                    state = NONE;
                }
                attempting = false;

                break;
        }
    }

    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.GAMEOBJECTS_LAYER;
    }
    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }
}
