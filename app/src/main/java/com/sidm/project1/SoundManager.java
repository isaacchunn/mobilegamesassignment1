package com.sidm.project1;

import android.content.Context;
import android.media.MediaPlayer;

public class SoundManager {

    //Write a singleton
    private static SoundManager Instance;
    public int paused;

    public static SoundManager getInstance()
    {
        if(Instance == null)
        {
            Instance = new SoundManager();
        }
        return Instance;
    }

    public MediaPlayer player;

    public void play(Context context, int resId, MediaPlayer.OnCompletionListener completionListener, MediaPlayer.OnErrorListener errorListener, Boolean isLoop)
    {
        if(player != null)
        {
            if(player.isPlaying())
            {
                player.stop();
            }
            player.release();
            player = null;
        }

        reset();
        player = MediaPlayer.create(context, resId);
        player.setOnCompletionListener(completionListener);
        player.setOnErrorListener(errorListener);
        player.setLooping((isLoop));
        player.start();

    }

    public void play(Context context , int resId, Boolean isLoop)
    {
        play(context,resId,null,null,isLoop);
    }

    public void play(Context context, int resId, MediaPlayer.OnCompletionListener completionListener ,Boolean isLoop)
    {
        play(context,resId,completionListener,null,isLoop);
    }

    public void play(Context context, int resId ,MediaPlayer.OnErrorListener errorListener ,Boolean isLoop)
    {
        play(context,resId,null,errorListener,isLoop);
    }

    public void resume(Context context)
    {
        player.seekTo(paused);
        player.start();
    }

    public void reset()
    {
        paused = 0;
    }

    public void pause()
    {
        if(player != null) {
            player.pause();
            paused = player.getCurrentPosition();
        }
    }

    public void stop()
    {
        stopPlayer();
    }

    private void stopPlayer()
    {
        if(player != null)
        {
            player .stop();
            player.release();
            player = null;
        }
    }

}
