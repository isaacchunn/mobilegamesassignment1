package com.sidm.project1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Grid {
    public final static Grid Instance = new Grid();
    private Grid() {
    }


    private final int worldSizeX = 30;
    private final int worldSizeY  = 16;
    Node[][] grid;
    public ArrayList<Node>path;
    public ArrayList<Node>walkableList = new ArrayList<>();

    void CreateGrid(int mapNumber)
    {
        int gridSize = GameDataManager.Instance.GetGridSize();
        int noOfGrid = GameDataManager.Instance.GetNoOfGrid();

        Map<Integer,Boolean> walkable =  DataManager.Instance.dataMap.get(mapNumber).walkableMap;

       grid = new Node[worldSizeX][worldSizeY];

       for(Map.Entry<Integer,Boolean> entry : walkable.entrySet())
       {
           Integer index = entry.getKey();
           Boolean bWalkable = entry.getValue();
           int gridX = index % noOfGrid;
           int gridY = index/noOfGrid;
           grid[gridX][gridY] = new Node(bWalkable, new Vector3(gridX * gridSize, (gridY)* gridSize),gridX,gridY);

           if(bWalkable)
           {
               walkableList.add(new Node(bWalkable, new Vector3(gridX * gridSize, (gridY)* gridSize),gridX,gridY));
           }
       }
    }
    public ArrayList<Node> GetNeighbours (Node node)
    {
        ArrayList<Node>neighbours = new ArrayList<Node>();
        for(int x = -1; x <= 1 ; ++x)
        {
            for(int y = -1; y <= 1 ; ++y)
            {
                if(x ==0 && y ==0)
                    continue;

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if(checkX >= 0 && checkX < worldSizeX && checkY >=0 && checkY < worldSizeY)
                {
                    neighbours.add(grid[checkX][checkY]);
                }
            }
        }
        return neighbours;
    }

    void PrintGrid()
    {
        for (int i = 0; i < grid.length; ++i) {
            for(int j = 0; j < grid[i].length; ++j) {
                System.out.println(grid[i][j].gridX +"," +grid[i][j].gridY );
            }
        }
    }




}
