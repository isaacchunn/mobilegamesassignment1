package com.sidm.project1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Switch;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

public class Splashpage  extends Activity{

    protected boolean  _active = true;
    protected int _splashTime = 7500;

    Random rand = new Random();
    static private int textCycling = 0;
    private int textCycleTimer = 2500;

    private TextView textView;
    private TextView highscoreView;
    private TextView highscoreView2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splashpage);

        //Set the textView
        textView = (TextView)findViewById(R.id.infoBox);

        //Init the screen size and load all assets in the splash screen
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int xpixels = displayMetrics.widthPixels;
        int ypixels = displayMetrics.heightPixels;

        //Add your maps here
        DataManager.Instance.AddMap(DataReader.Instance.readCSV(this,R.raw.tilemap));
        DataManager.Instance.AddMap(DataReader.Instance.readCSV(this,R.raw.tilemaplevel2));
        DataManager.Instance.AddMap(DataReader.Instance.readCSV(this,R.raw.test));
        DataManager.Instance.AddMap(DataReader.Instance.readCSV(this,R.raw.test2));

        //Init the map number with the amount of pixels
        GameDataManager.Instance.Init(xpixels,ypixels,DataManager.Instance.ReturnData(0).numberOfGrid);
//        System.out.println(DataManager.Instance.dataMap.get(0).formattedData);
//        System.out.println(DataManager.Instance.dataMap.get(1).formattedData);

        //Create a gridf for the first map
        Grid.Instance.CreateGrid(0);

        DataSaver.Instance.sharedPref =getSharedPreferences("GameSaveFile", Context.MODE_PRIVATE);

        DataSaver.Instance.SaveEditBegin();
       DataSaver.Instance.SetIntInSave("highscorelevel1",0);
        DataSaver.Instance.SetIntInSave("highscorelevel2",0);
        DataSaver.Instance.SaveEditEnd();



        //thread for displaying the splash screen
        Thread splashThread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep(200);
                        if (_active) {
                            waited += 200;
                            textCycling += 200;

                            if(textCycling > textCycleTimer)
                            {
                                int n = rand.nextInt(5) + 1;

                                switch(n)
                                {
                                    case 1:
                                        textView.setText((R.string.info1));
                                        break;
                                    case 2:
                                        textView.setText((R.string.info2));
                                        break;
                                    case 3:
                                        textView.setText((R.string.info3));
                                        break;
                                    case 4:
                                        textView.setText((R.string.info4));
                                        break;
                                    case 5:
                                        textView.setText((R.string.info5));
                                        break;
                                }
                                textCycling = 0;

                            }
                        }
                    }
                } catch (InterruptedException e) {
                    //do nothing
                } finally {
                    finish();
                    //create new activity based on and intent with currentActivity

                    Intent intent = new Intent(Splashpage.this, Mainmenu.class);
                    startActivity(intent);
                }

            }
        };

        splashThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        //int action = MotionEvent.ACTION_DOWN;
        if(event.getAction() == MotionEvent.ACTION_DOWN)
        {
            _active = false;
        }
        return true;
    }
}
