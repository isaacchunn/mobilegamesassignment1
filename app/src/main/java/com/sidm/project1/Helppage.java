package com.sidm.project1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Helppage extends Activity {

    private ViewPager viewPager;
    private LinearLayout linearLayout;
    private TextView[] dots;
    private SliderAdapter sliderAdapter;

    private Button nextBtn;
    private Button prevBtn;
    private Button returnBtn;
    private int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);;

        setContentView(R.layout.helppage);

        viewPager = (ViewPager) findViewById(R.id.slideView);
        linearLayout  = (LinearLayout) findViewById(R.id.linearLayout);

        nextBtn = (Button)findViewById(R.id.optionForward);
        prevBtn = (Button)findViewById(R.id.optionBack);
        returnBtn = (Button)findViewById(R.id.btn_return);

        sliderAdapter = new SliderAdapter(this);
        viewPager.setAdapter((sliderAdapter));

        addDotsIndicator(0);
        viewPager.addOnPageChangeListener(viewListener);

        nextBtn.setOnClickListener(new View.OnClickListener(){
            @Override
                    public void onClick(View v) {
                viewPager.setCurrentItem(currentPage+1);
            }

        });

        prevBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(currentPage-1);
            }

        });

        returnBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent backIntent = new Intent();
                backIntent.setClass(Helppage.this, Mainmenu.class);
                startActivity(backIntent);
            }

        });
    }

    public void addDotsIndicator(int _position)
    {
        dots = new TextView[5];
        linearLayout.removeAllViews();

        for(int i =0; i < dots.length; i++)
        {
                dots[i] = new TextView(this);

                dots[i].setText(Html.fromHtml("&#8226;",Html.FROM_HTML_MODE_LEGACY));
                dots[i].setTextSize(35);
                dots[i].setTextColor(getColor(R.color.colorTransparentWhite));
            linearLayout.addView(dots[i]);
        }
        if(dots.length > 0)
        {
                dots[_position].setTextColor(Color.WHITE);
        }
    }

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {
                addDotsIndicator(i);
                currentPage= i;

                if( i ==0)
                {
                    nextBtn.setEnabled(true);
                    prevBtn.setEnabled(false);
                    prevBtn.setVisibility(View.INVISIBLE);

                    nextBtn.setText((R.string.Next));
                    prevBtn.setText("");
                }
                else if (i == dots.length - 1)
                {
                    nextBtn.setEnabled(true);
                    prevBtn.setEnabled(true);
                    prevBtn.setVisibility(View.VISIBLE);

                    nextBtn.setText(("Finish"));
                    prevBtn.setText(R.string.Prev);
                }
                else
                {
                    nextBtn.setEnabled(true);
                    prevBtn.setEnabled(true);
                    prevBtn.setVisibility(View.VISIBLE);

                    nextBtn.setText((R.string.Next));
                    prevBtn.setText(R.string.Prev);
                }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    };

    @Override
    protected void onStop() {
        super.onStop();
    }

}
