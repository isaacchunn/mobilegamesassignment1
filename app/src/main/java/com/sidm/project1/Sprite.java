package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class Sprite {

    private int row = 0;
    private int col = 0;
    private int width = 0;
    private int height = 0;

    private Bitmap bmp = null;

    private int currentFrame = 0;
    private int startFrame = 0;
    private int endFrame = 0;

    private float timePerFrame = 0.0f;
    private float timeAcc = 0.0f;

    private Boolean repeat = true;
    private Boolean done = false;

    private float alpha = 255;

    public Sprite(Bitmap _bmp, int _row, int _col, int _fps) {
        bmp = _bmp;
        row = _row;
        col = _col;
        width = bmp.getWidth() / col;
        height = bmp.getHeight() / row;
        timePerFrame = 1.0f / (float) _fps;

        endFrame = _col * _row;

    }

    public void Update(float _dt) {

        if(!done)
        {
        timeAcc += _dt;
        if (timeAcc > timePerFrame) {
            ++currentFrame;
            if (currentFrame >= endFrame) {
                if (!repeat) {
                    done = true;
                }
                currentFrame = startFrame;
            }
            timeAcc = 0.0f;
        }
        }

    }

    public void Render(Canvas _canvas, int _x, int _y) {
        int frameX = currentFrame % col;
        int frameY = currentFrame / col;
        int srcX = frameX * width;
        int srcY = frameY * height;

        _x -= 0.5f * width;
        _y -= 0.5 * height;

        Rect src = new Rect(srcX, srcY, srcX + width, srcY + height);
        Rect dst = new Rect(_x, _y, _x + width, _y + height);

        Paint paint = new Paint();
        paint.setAlpha((int)alpha);
        _canvas.drawBitmap(bmp, src, dst, paint);

    }

    public void SetAnimationFrames(int _start, int _end) {
        timeAcc = 0.0f;
        currentFrame = _start;
        startFrame = _start;
        endFrame = _end;
    }

    public int GetHeight() {
        return height;
    }

    public int GetWidth() {
        return width;
    }

    public void SetAlpha(float alpha)
    {
        this.alpha = alpha;
    }

    public void SetAlpha(int alpha)
    {
        this.alpha = alpha;
    }

    public float GetAlpha()
    {
        return this.alpha;
    }

    public boolean getRepeat()
    {
        return this.repeat;
    }

    public void setRepeat(Boolean bool)
    {
        this.repeat = bool;
    }

    public boolean getDone()
    {
        return this.done;
    }

    public void setDone(Boolean bool)
    {
        this.done = bool;
    }



}
