package com.sidm.project1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.SurfaceView;

public class RenderBackground implements EntityBase {

    private boolean isInit = false;
    private Bitmap bmp = null;
    private Sprite spriteSheet = null;
    boolean isDone = false;
    private  float xPos, yPos;
    int ScreenWidth=0,ScreenHeight=0;
    DisplayMetrics metrics = new DisplayMetrics();
    float offset = 0;
    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    @Override
    public boolean IsInit() {
        return isInit;
    }

    @Override
    public void Init(SurfaceView _view)
    {
        // Define anything you need to use here
        //spriteSheet = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.megamanv3), 1,4,16);
        spriteSheet = new Sprite(ResourceManager.Instance.GetBitmap(R.drawable.tilemap), 1,4,16);
        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.ship2_1); //other images

//
//        xPos =(float)(ScreenWidth*0.2);
//        yPos =(float)(ScreenHeight*0.5);
    }

    @Override
    public void Update(float _dt)
    {
        //Use dt to do some update or run something
        /*offset += _dt;
        if (offset >= 1.5f) {
            //offset = 0;
        }*/
         xPos -= _dt * 500;

        if (xPos < -ScreenWidth) {
            xPos = 0;
        }
    }

    @Override
    public void Render(Canvas _canvas)
    {
        //_canvas.drawBitmap(bmp, xPos, yPos, null); //left = x, top = y, counting from top left corner(0,0)
    }

    //new compared to other entites?
    @Override
    public int GetRenderLayer()
    {
        return LayerConstants.BACKGROUND_LAYER;
    }

    @Override
    public void SetRenderLayer(int _newLayer)
    {
        return;
    }

    public static RenderBackground Create()
    {
        RenderBackground result = new RenderBackground();
        EntityManager.Instance.AddEntity(result);
        return result;
    }
}
