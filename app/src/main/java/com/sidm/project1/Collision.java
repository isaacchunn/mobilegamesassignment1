package com.sidm.project1;

public class Collision {

    public static boolean SphereToSphere(float x1, float y1, float radius1, float x2, float y2, float radius2) {
        float xVec = x2 - x1;
        float yVec = y2 - y1;
        float distSquared = xVec * xVec + yVec * yVec;
        float rSquared = radius1 + radius2;
        rSquared *= rSquared;

        if (distSquared > rSquared) {
            return false;
        }
        return true;

    }

    //Vector3 version
    public static boolean SphereToSphere(Vector3 pos1, Vector3 pos2, float radius) {
        float distanceSquared =pos1.CalculateDistanceSquared(pos2);

        if(distanceSquared < radius * radius)
        {
            return true;
        }
        return false;
    }
}
