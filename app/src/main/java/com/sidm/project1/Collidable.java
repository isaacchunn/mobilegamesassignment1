package com.sidm.project1;

public interface Collidable {

    String GetType();

    float GetPosX();
    float GetPosY();

    float GetRadius();

    void OnHit(Collidable _other);
    void TouchHit();
    boolean GetAttending();

}