package com.sidm.project1;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;

public class Mainmenu extends Activity implements View.OnClickListener{
    //Define object button and pass it to another method to use
    private Button btn_play;
    private Button btn_options;
    private Button btn_help;
    private Button pop_up;
    private   int backButtonCount = 0;

    private TextView highscoreView;
    private TextView highscoreView2;

    private        Switch soundswitch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.mainmenu);

        //to make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // hide title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.mainmenu);

        //Set Listener to Buttons
        btn_play = (Button)findViewById(R.id.btn_play);
        btn_play.setOnClickListener(this);

        btn_options = (Button)findViewById(R.id.btn_options);
        btn_options.setOnClickListener(this);

        btn_help = (Button)findViewById(R.id.btn_help);
        btn_help.setOnClickListener(this);

        pop_up = (Button) findViewById(R.id.pop_up);
        pop_up.setOnClickListener(this);

      SoundManager.getInstance().play(getApplicationContext(), R.raw.bensound_littleidea, true);



    }

    @Override
    // A callback method
    public void onClick(View v) {
        //Intent = action to be performed
        //Intent is an object that you need to create and a new instance to use it
        Intent intent = new Intent();
        if(v == btn_play)
        {
            //StateManager.Instance.ChangeState("SampleGame");
            intent.setClass(this, GamePage.class);
            SoundManager.getInstance().player = null;
        }
        else if(v == btn_options)
        {
            intent.setClass(this, Optionspage.class);
        }
        else if(v == btn_help) {
            intent.setClass(this, Helppage.class);
        }
        else if(v == pop_up)
        {
            intent.setClass(this, scorepage.class);
        }
       // SoundManager.getInstance().pause();
        startActivity(intent);
/*
        finish();
*/
    }

    @Override
    public void onBackPressed()
    {
        if (isTaskRoot()) {
        if(backButtonCount >= 1)
        {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            } else
                {
                Toast.makeText(this, "Press the back button once again to close the application.", Toast.LENGTH_SHORT).show();
                backButtonCount++;
            }
        } else
            {
            super.onBackPressed();
        }
    }

}
