package com.sidm.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class GamePage extends Activity /*implements View.OnClickListener*/ {

    /*
        private Button pop_up;
    */
    public final static GamePage Instance = new GamePage();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //to make fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE); // hide title bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(new GameView(this)); // Surfaceview - GameView

        //startActivity(new Intent(GamePage.this, PopupMessageWindow.class));

       /* pop_up = (Button) findViewById(R.id.pop_up);
        pop_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GamePage.this, PopupMessageWindow.class));
            }
        });*/

/*
       pop_up = (Button) findViewById(R.id.pop_up);
*/
/*
       pop_up.setOnClickListener(this);
*/
    }

    /*
        @Override
        public void onClick(View v)
        {
            startActivity(new Intent(GamePage.this, PopupMessageWindow.class));
        }
    */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();

        TouchManager.Instance.Update(x, y, event.getAction());

        //startActivity(new Intent(GamePage.this, PopupMessageWindow.class));
        for (int i = 0; i < EntityManager.Instance.entityList.size(); ++i) {
            EntityBase currEntity = EntityManager.Instance.entityList.get(i);

            if (currEntity instanceof Collidable) {
                Collidable first = (Collidable) currEntity;
                if (TouchManager.Instance.HasTouch()) {
                    if (first.GetType() == "Entity_PauseButton") {
                        if (Collision.SphereToSphere(first.GetPosX(), first.GetPosY(), first.GetRadius(), (float) x, (float) y, 50)) {
//                            startActivity(new Intent(GamePage.this, PopupMessageWindow.class));
                            openMenu();
                        }
                    }
                }
            }
        }
        return true;
    }

    public void openMenu() {
        startActivity(new Intent(GamePage.this, PopupMessageWindow.class));
    }
}
