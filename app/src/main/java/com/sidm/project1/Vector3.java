package com.sidm.project1;

public class Vector3 {

    //The x component of the vector
    public float x;
    //The y component of the vector
    public float y;
    //The z component of the vector
    public float z;

    public Vector3()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    //Constructor for a 3d vector
    public Vector3(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //2d implementation of Vector3
    public Vector3(float x, float y)
    {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    //Equals constructor or just set
    public void Set(Vector3 other)
    {
        this.x = other.x;
        this.y = other.y;
        this.z = other.z;
    }

    //Set using a vector
    public void Set(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //Get a copy of the vector
    public Vector3 Get()
    {
        return new Vector3(x,y,z);
    }

    //Calculate magnitude of vector and return it
    public float Length()
    {
        return (float)Math.sqrt(x*x + y*y + z*z);
    }

    //Add a vector to this vector
    public void Add(Vector3 other)
    {
        this.x += other.x;
        this.y +=other.y;
        this.z += other.z;
    }

    //Add floats to this vector
    public void Add(float x, float y, float z)
    {
        this.x += x;
        this.y += y;
        this.z += z;
    }

    //Add two vectors and return it as the sum of both
    static public Vector3 Add(Vector3 other, Vector3 other2)
    {
        return Add(other, other2, null);

    }

    //Add two vectors into a target vector
    static public Vector3 Add (Vector3 other, Vector3 other2, Vector3 target)
    {
        if(target == null)
        {
            target = new Vector3(other.x + other2.x, other.y + other2.y, other.z + other2.z);
        }
        else
        {
            target.Set(other.x + other2.x, other.y + other2.y, other.z + other2.z);
        }
        return target;
    }


    //Subtracting a vector from this vector
    public void Sub(Vector3 other)
    {
            this.x -= other.x;
            this.y -= other.y;
            this.z -= other.z;
    }

    //Subtracting this vector from floats
    public void Sub(float x, float y, float z)
    {
        this.x -= x;
        this.y -= y;
        this.z -= z;
    }

    //Subtracting one vector from another vector
    static public Vector3  Sub(Vector3 other, Vector3 other2)
    {
        return Sub(other,other2,null);
    }

    //Return two vectors subtracted into a targeted vector
    static public Vector3 Sub(Vector3 other, Vector3 other2, Vector3 target)
    {
        if(target == null)
        {
            target = new Vector3(other.x - other2.x, other.y - other2.y, other.z - other2.z);
        }
        else
        {
            target.Set(other.x - other2.x, other.y - other2.y, other.z - other2.z);
        }
        return target;
    }

    //Multiply this vector with a scalar value
    public void Mult(float scalar)
    {
        x *= scalar;
        y*= scalar;
        z *=scalar;
    }

    static public Vector3 Mult(Vector3 other, Vector3 other2)
    {
        return Mult(other,other2, null);
    }

    static public Vector3 Mult(Vector3 other, Vector3 other2, Vector3 target)
    {
        if(target == null)
        {
            target = new Vector3(other.x * other2.x, other.y*other2.y, other.z * other2.z);
        }
        else
        {
            target.Set(other.x * other2.x, other.y*other2.y, other.z * other2.z);
        }

        return target;
    }

    //Divide the vector with a scalar
    //I didnt really want to type this out lol who even uses divide anyway
    public void Div(float scalar)
    {
        this.x /= scalar;
        this.y/= scalar;
        this.z/=scalar;
    }

    static public Vector3 Div(Vector3 other, float scalar)
    {
        return Div(other, scalar, null);
    }

    //Do division between this vector and return it as a target;
    static public Vector3 Div(Vector3 other, float scalar, Vector3 target)
    {
        if(target == null)
        {
            target = new Vector3(other.x/scalar,other.y/scalar,other.z/scalar);
        }
        else
        {
            target .Set(other.x/scalar,other.y/scalar,other.z/scalar);
        }
        return target;
    }

    //Calculate the distance between two vectors
    public float CalcualteDistance(Vector3 other)
    {
        float dx = x - other.x;
        float dy = y - other.y;
        float dz = z - other.z;
        return (float) Math.sqrt(dx * dx + dy*dy + dz*dz);
    }

    //Calculate distance between two vectors and return a float
    static public float CalculateDistance(Vector3 other, Vector3 other2)
    {
        float dx = other.x - other2.x;
        float dy = other.y- other2.y;
        float dz = other.z - other2.z;
        return (float) Math.sqrt(dx * dx + dy*dy + dz*dz);
    }

    //Calculate the distance between two vectors
    public float CalculateDistanceSquared(Vector3 other)
    {
        float dx = x - other.x;
        float dy = y - other.y;
        float dz = z - other.z;
        return (float) (dx * dx + dy*dy + dz*dz);
    }

    //Calculate distance between two vectors and return a float
    static public float CalculateDistanceSquared(Vector3 other, Vector3 other2)
    {
        float dx = other.x - other2.x;
        float dy = other.y- other2.y;
        float dz = other.z - other2.z;
        return (float) (dx * dx + dy*dy + dz*dz);
    }


    //Calculate the dot product with another vector and return it
    public float Dot(Vector3 other)
    {
        return x*other.x + y*other.y +z*other.z;
    }

    public float Dot(float x, float y, float z)
    {
        return x*this.x + y*this.y +z*this.z;
    }

    static public float Dot(Vector3 other, Vector3 other2)
    {
        return (other.x * other2.x + other.y*other2.y + other.z*other2.z);
    }

    //Return a vector of the cross product between this vector and given vector
    public Vector3 Cross(Vector3 other)
    {
        return Cross(other, null);
    }

    //Perform cross product between this and other vector, and store result in target
    public Vector3 Cross(Vector3 other, Vector3 target)
    {
        float crossX = y * other.z - other.y * z;
        float crossY = z * other.x - other.z * x;
        float crossZ = x * other.y - other.x * y;

        if(target == null)
        {
            target = new Vector3(crossX,crossY,crossZ);
        }
        else
        {
            target.Set(crossX,crossY,crossZ);
        }
        return target;
    }

    static public Vector3 Cross(Vector3 other, Vector3 other2, Vector3 target)
    {
        float crossX = other.y * other2.z - other2.y * other.z;
        float crossY = other.z * other2.x - other2.z * other.x;
        float crossZ = other.x * other2.y - other2.x * other.y;

        if(target == null)
        {
            target = new Vector3(crossX,crossY,crossZ);
        }
        else
        {
            target.Set(crossX,crossY,crossZ);
        }
        return target;

    }

    //Normalize the vector to length 1
    public void Normalize()
    {
        float l = Length();
        if(l !=0 && l !=1)
        {
            Div(l);
        }
    }

    //Normalize this vector and store into another vector
    public Vector3 Normalize(Vector3 target)
    {
        if(target == null)
        {
            target= new Vector3();
        }
        float  l = Length();
        if(l > 0)
        {
            target.Set(x/l,y/l,z/l);
        }
        else
        {
            target.Set(x,y,z);
        }
        return target;
    }

    //Limit the length of this vector
    public void Limit(float maxValue)
    {
        if(Length() > maxValue)
        {
            Normalize();
            Mult(maxValue);
        }
    }

    //Calculate the angle of rotation for this vector for 2D vectors
    public float AngleBetween2D()
    {
        float angle = (float)Math.atan2(-y,x);
        return -1 * angle;
    }


    //Calculate the angle between two vectors using dot product
    static public float AngleBetween(Vector3 other, Vector3 other2)
    {
        double dot = other.x * other2.x + other.y *other2.y + other.z * other2.z;
        double otherMag = Math.sqrt(other.x  * other.x + other.y * other.y + other.z * other.z);
        double other2Mag = Math.sqrt(other2.x * other2.x + other2.y * other2.y + other2.z * other2.z);
        return (float)Math.acos(dot / (otherMag*other2Mag));
    }

    public String toString()
    {
        return "[ " + x + ", " + y + ", " + z + " ]";
    }

    //Prints output of this vector into console
    public void Print()
    {
        System.out.println("[ " + x + ", " + y + ", " + z + " ]");
    }

    //Prints the output of the other vector into the console
    public void Print(Vector3 other)
    {
        System.out.println("[ " + other.x + ", " + other.y + ", " + other.z + " ]");
    }


















}
