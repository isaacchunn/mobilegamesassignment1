package com.sidm.project1;

import java.util.ArrayList;

public class GameDataManager {

    public final static GameDataManager Instance = new GameDataManager();
    private GameDataManager() {
    }

    private int ScreenWidth; // screen width
    private int ScreenHeight; //height

    private int gridSize; // splitting the phone screen into sections
    private int noOfGrid;   // the amount of grids in the world
    private int gridOffset; // to offset the grids

    private int  currentRubbish = 0;
    private int maxRubbish = 8;

    private float hourOfTheDay;

    public float energyLeft;

    public ArrayList<Entity_Interactables>interactableList = new ArrayList<>();

    public void Init(int screenWidth, int screenHeight, int noOfGrid)
    {
        //Init our values that we should call in the Splashpage class
        //Call this after we have gotten our ScreenWidths and ScreenHeights;
        this.ScreenWidth = screenWidth;
        this.ScreenHeight = screenHeight;
        this.noOfGrid = noOfGrid;
        //Apply offsetting.... if we need to but now we dont..
        //Do offset code
        //End of offset
        gridSize = ScreenWidth/ noOfGrid;
        gridOffset = gridSize /2;
    }

    public void SetScreenWidth(int ScreenWidth)
    {
        this.ScreenWidth = ScreenWidth;
    }
    public int GetScreenWidth()
    {
        return ScreenWidth;
    }
    public void SetScreenHeight(int ScreenHeight)
    {
        this.ScreenHeight = ScreenHeight;
    }
    public int GetScreenHeight()
    {
        return ScreenHeight;
    }
    public void SetNoOfGrid(int noOfGrid)
    {
        this.noOfGrid = noOfGrid;
    }
    public int GetNoOfGrid()
    {
        return noOfGrid;
    }
    public void SetGridOffset(int gridOffset)
    {
        this.gridOffset = gridOffset;
    }
    public int GetGridOffset()
    {
        return gridOffset;
    }
    public void SetGridSize(int gridSize)
    {
        this.gridSize = gridSize;
    }

    public int GetGridSize()
    {
        return gridSize;
    }


    public float getHourOfTheDay() {
        return hourOfTheDay;
    }
    public void setHourOfTheDay(float hourOfTheDay) {
        this.hourOfTheDay = hourOfTheDay;
    }


    public float getEnergyLeft() {
        return energyLeft;
    }

    public void setEnergyLeft(float energyLeft) {
        this.energyLeft = energyLeft;
    }

    public void AddInteractable(Entity_Interactables _newEntity) {
        interactableList.add(_newEntity);
    }
    public void RemoveInteractble(Entity_Interactables _newEntity) {
        interactableList.remove(_newEntity);
    }


    public int GetMaxRubbish()
    {
        return maxRubbish;
    }

    public void SetMaxRubbish(int maxRubbish)
    {
        this.maxRubbish = maxRubbish;
    }

    public int GetCurrentRubbish()
    {
        return currentRubbish;
    }

    public void SetCurrentRubbish(int currentRubbish)
    {
        this.currentRubbish = currentRubbish;
    }

}
